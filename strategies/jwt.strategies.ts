import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from 'src/modules/users/entities/user.entity';
import { UserRepository } from 'src/modules/users/user.repository';
import { JwtPayload } from 'src/jwt/jwt-payload-interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'Qwertyuiop1234567890',
    }); // It will call the contructor of the base class to which we are calling this accept some configs how the strategy works of passport JS
  }
  async validate(payload: JwtPayload): Promise<User> {
    const { email } = payload;
    const user = await this.userRepository.findOne({ email });
    user['fullname'] = user.firstName + ' ' + user.lastName;
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
