import { Strategy } from 'passport-jwt';
import { User } from 'src/modules/users/entities/user.entity';
import { UserRepository } from 'src/modules/users/user.repository';
import { JwtPayload } from 'src/jwt/jwt-payload-interface';
declare const JwtStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    private userRepository;
    constructor(userRepository: UserRepository);
    validate(payload: JwtPayload): Promise<User>;
}
export {};
