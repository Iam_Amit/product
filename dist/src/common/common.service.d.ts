import { User } from 'src/modules/users/entities/user.entity';
export declare class CommonService {
    randomNumber(length: number): string;
    randomString(length: number): string;
    iterateString(string: any, length: any): string;
    checkRoleAdmin(user: User): boolean;
    checkRoleUser(user: User): boolean;
}
