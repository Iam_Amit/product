"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const platform_express_1 = require("@nestjs/platform-express");
const typeorm_1 = require("@nestjs/typeorm");
const Joi = require("joi");
const path_1 = require("path");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const mail_module_1 = require("./mail/mail.module");
const address_module_1 = require("./modules/address/address.module");
const blog_module_1 = require("./modules/blog/blog.module");
const component_module_1 = require("./modules/component/component.module");
const dashboard_module_1 = require("./modules/dashboard/dashboard.module");
const faq_module_1 = require("./modules/faq/faq.module");
const fileupload_module_1 = require("./modules/fileUpload/fileupload.module");
const order_module_1 = require("./modules/order/order.module");
const page_module_1 = require("./modules/pages/page.module");
const product_module_1 = require("./modules/product/product.module");
const testimonial_module_1 = require("./modules/testimonial/testimonial.module");
const transaction_module_1 = require("./modules/transaction/transaction.module");
const users_module_1 = require("./modules/users/users.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            platform_express_1.MulterModule.register({
                dest: './uploads',
            }),
            config_1.ConfigModule.forRoot({
                envFilePath: '.env',
                isGlobal: true,
                validationSchema: Joi.object({
                    TWILIO_SENDER_PHONE_NUMBER: Joi.string().required(),
                    TWILIO_ACCOUNT_SID: Joi.string().required(),
                    TWILIO_AUTH_TOKEN: Joi.string().required(),
                    TWILIO_VERIFICATION_SERVICE_SID: Joi.string().required(),
                }),
            }),
            users_module_1.UsersModule,
            blog_module_1.BlogModule,
            faq_module_1.FaqModule,
            product_module_1.ProductModule,
            fileupload_module_1.FileuploadModule,
            component_module_1.ComponentModule,
            order_module_1.OrderModule,
            address_module_1.AddressModule,
            testimonial_module_1.TestimonialModule,
            page_module_1.PageModule,
            mail_module_1.MailModule,
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                port: 5432,
                host: 'localhost',
                username: 'postgres',
                password: 'Qwerty@!#456',
                database: 'e_product',
                entities: [(0, path_1.join)(__dirname, './**/**.entity{.ts,.js}')],
                migrationsTableName: 'Migrations_History',
                synchronize: true,
            }),
            transaction_module_1.TransactionModule,
            dashboard_module_1.DashboardModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map