import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateAddressDto } from './dto/create-address.dto';
import { Address } from './entities/address.entity';
export declare class AddressRepository extends Repository<Address> {
    createAddress(createAddressDto: CreateAddressDto, user: User): Promise<Address>;
    listAddress(): Promise<Address[]>;
}
