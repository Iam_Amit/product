"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const address_repository_1 = require("./address.repository");
let AddressService = class AddressService {
    constructor(addressRepo) {
        this.addressRepo = addressRepo;
    }
    create(createAddressDto, user) {
        return this.addressRepo.createAddress(createAddressDto, user);
    }
    findAll(user) {
        return this.addressRepo.find({ where: { user: user } });
    }
    async findOne(id) {
        const found = await this.addressRepo.findOne(id);
        if (!found) {
            throw new common_1.NotFoundException(`Address with the id ${id} not found`);
        }
        return found;
    }
    update(id, updateAddressDto) {
        return this.addressRepo.update(id, updateAddressDto);
    }
    async remove(id) {
        const result = await this.addressRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`Address with the id ${id} not found`);
        }
    }
};
AddressService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(address_repository_1.AddressRepository)),
    __metadata("design:paramtypes", [address_repository_1.AddressRepository])
], AddressService);
exports.AddressService = AddressService;
//# sourceMappingURL=address.service.js.map