import { Order } from 'src/modules/order/entities/order.entity';
import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
export declare class Address extends BaseEntity {
    id: number;
    address: string;
    city: string;
    state: string;
    streetNo: string;
    latitude: string;
    longitude: string;
    zipCode: number;
    deletedAt?: Date;
    createdAt: Date;
    updatedAt: Date;
    user: User;
    orders: Order[];
}
