import { User } from '../users/entities/user.entity';
import { AddressRepository } from './address.repository';
import { CreateAddressDto } from './dto/create-address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';
import { Address } from './entities/address.entity';
export declare class AddressService {
    private addressRepo;
    constructor(addressRepo: AddressRepository);
    create(createAddressDto: CreateAddressDto, user: User): Promise<Address>;
    findAll(user: User): Promise<Address[]>;
    findOne(id: number): Promise<Address>;
    update(id: number, updateAddressDto: UpdateAddressDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
}
