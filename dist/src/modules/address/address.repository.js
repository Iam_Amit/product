"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const address_entity_1 = require("./entities/address.entity");
let AddressRepository = class AddressRepository extends typeorm_1.Repository {
    async createAddress(createAddressDto, user) {
        const { location, city, state, streetNo, latitude, longitude, zipCode, } = createAddressDto;
        const address = new address_entity_1.Address();
        address.address = location;
        address.city = city;
        address.state = state;
        address.streetNo = streetNo;
        address.latitude = latitude;
        address.longitude = longitude;
        address.zipCode = zipCode;
        address.user = user;
        try {
            await address.save();
            delete address.updatedAt;
            delete address.deletedAt;
            return address;
        }
        catch (e) {
            console.log(e);
            throw new common_1.InternalServerErrorException('Error While saving Address');
        }
    }
    async listAddress() {
        const query = this.createQueryBuilder('address');
        const address = await query.getMany();
        return address;
    }
};
AddressRepository = __decorate([
    (0, typeorm_1.EntityRepository)(address_entity_1.Address)
], AddressRepository);
exports.AddressRepository = AddressRepository;
//# sourceMappingURL=address.repository.js.map