export declare class CreateComponentDto {
    id: number;
    name: string;
    description: string;
    smallDescription: string;
    photo: string;
    video: string;
    price: string;
    quantity: string;
}
