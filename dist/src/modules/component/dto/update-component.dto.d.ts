export declare class UpdateComponentDto {
    name: string;
    description: string;
    photo: string;
    video: string;
    price: string;
}
