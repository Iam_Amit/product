import { OrderItem } from 'src/modules/order/entities/orderItem.entity';
import { Testimonial } from 'src/modules/testimonial/entities/testimonial.entity';
import { BaseEntity } from 'typeorm';
export declare class Component extends BaseEntity {
    id: number;
    name: string;
    description: string;
    smallDescription: string;
    photo: string;
    video: string;
    quantity: string;
    price: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt?: Date;
    orderItems: OrderItem[];
    testimonials: Testimonial[];
}
