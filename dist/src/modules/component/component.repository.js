"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ComponentRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const component_entity_1 = require("./entities/component.entity");
let ComponentRepository = class ComponentRepository extends typeorm_1.Repository {
    async createComponent(createComponentDto) {
        const { name, description, smallDescription, price, photo, video, quantity, } = createComponentDto;
        const component = new component_entity_1.Component();
        component.name = name;
        component.description = description;
        component.smallDescription = smallDescription;
        component.price = price;
        component.photo = photo;
        component.video = video;
        component.quantity = quantity;
        try {
            await component.save();
            delete component.createdAt;
            delete component.deletedAt;
            delete component.updatedAt;
            return component;
        }
        catch (e) {
            throw new common_1.InternalServerErrorException('Error While saving Component ');
        }
    }
    async listComponent(search, page, limit) {
        const query = this.createQueryBuilder('component');
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('component.name LIKE :search OR component.description LIKE :search', { search: `%${search}%` });
        }
        const component = await query.getManyAndCount();
        console.log('component => ', component);
        return component;
    }
};
ComponentRepository = __decorate([
    (0, typeorm_1.EntityRepository)(component_entity_1.Component)
], ComponentRepository);
exports.ComponentRepository = ComponentRepository;
//# sourceMappingURL=component.repository.js.map