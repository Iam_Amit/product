import { ProductRepository } from '../product/product.repository';
import { ComponentRepository } from './component.repository';
import { CreateComponentDto } from './dto/create-component.dto';
import { UpdateComponentDto } from './dto/update-component.dto';
import { Component } from './entities/component.entity';
export declare class ComponentService {
    private componentRepo;
    private productRepo;
    constructor(componentRepo: ComponentRepository, productRepo: ProductRepository);
    create(createComponentDto: CreateComponentDto): Promise<Component>;
    findAll(page: number, limit: number, search: string): Promise<{
        results: Component[];
        searchTotal: number;
        total: number;
    }>;
    findOne(id: number): Promise<Component>;
    update(id: number, updateComponentDto: UpdateComponentDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
}
