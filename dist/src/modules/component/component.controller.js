"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ComponentController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const common_service_1 = require("../../common/common.service");
const component_service_1 = require("./component.service");
const create_component_dto_1 = require("./dto/create-component.dto");
const update_component_dto_1 = require("./dto/update-component.dto");
let ComponentController = class ComponentController {
    constructor(componentService, cs) {
        this.componentService = componentService;
        this.cs = cs;
    }
    async create(createComponentDto, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.componentService.create(createComponentDto);
    }
    async findAll(page, limit, search, res) {
        const val = await this.componentService.findAll(parseInt(page) - 1 || 0, parseInt(limit) || 10, search);
        return res.status(common_1.HttpStatus.OK).send(val);
    }
    findOne(id) {
        return this.componentService.findOne(id);
    }
    async update(id, updateComponentDto, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.componentService.update(+id, updateComponentDto);
    }
    async remove(id, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.componentService.remove(id);
    }
};
__decorate([
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_component_dto_1.CreateComponentDto, Object]),
    __metadata("design:returntype", Promise)
], ComponentController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('page')),
    __param(1, (0, common_1.Query)('limit')),
    __param(2, (0, common_1.Query)('search')),
    __param(3, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], ComponentController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ComponentController.prototype, "findOne", null);
__decorate([
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_component_dto_1.UpdateComponentDto, Object]),
    __metadata("design:returntype", Promise)
], ComponentController.prototype, "update", null);
__decorate([
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], ComponentController.prototype, "remove", null);
ComponentController = __decorate([
    (0, common_1.Controller)('component'),
    __metadata("design:paramtypes", [component_service_1.ComponentService,
        common_service_1.CommonService])
], ComponentController);
exports.ComponentController = ComponentController;
//# sourceMappingURL=component.controller.js.map