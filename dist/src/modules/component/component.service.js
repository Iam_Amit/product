"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ComponentService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const product_repository_1 = require("../product/product.repository");
const component_repository_1 = require("./component.repository");
let ComponentService = class ComponentService {
    constructor(componentRepo, productRepo) {
        this.componentRepo = componentRepo;
        this.productRepo = productRepo;
    }
    async create(createComponentDto) {
        return this.componentRepo.createComponent(createComponentDto);
    }
    async findAll(page, limit, search) {
        const [output, searchTotal] = await this.componentRepo.listComponent(search, page, limit);
        const total = await this.componentRepo.count();
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            return x;
        });
        return {
            results,
            searchTotal,
            total,
        };
    }
    async findOne(id) {
        const found = await this.componentRepo.findOne(id);
        if (!found) {
            throw new common_1.NotFoundException(`Component with the id ${id} not found`);
        }
        return found;
    }
    update(id, updateComponentDto) {
        return this.componentRepo.update(id, updateComponentDto);
    }
    async remove(id) {
        const result = await this.componentRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`Component with the id ${id} not found`);
        }
    }
};
ComponentService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(component_repository_1.ComponentRepository)),
    __param(1, (0, typeorm_1.InjectRepository)(product_repository_1.ProductRepository)),
    __metadata("design:paramtypes", [component_repository_1.ComponentRepository,
        product_repository_1.ProductRepository])
], ComponentService);
exports.ComponentService = ComponentService;
//# sourceMappingURL=component.service.js.map