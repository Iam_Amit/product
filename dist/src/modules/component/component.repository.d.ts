import { Repository } from 'typeorm';
import { CreateComponentDto } from './dto/create-component.dto';
import { Component } from './entities/component.entity';
export declare class ComponentRepository extends Repository<Component> {
    createComponent(createComponentDto: CreateComponentDto): Promise<Component>;
    listComponent(search: string, page: number, limit: number): Promise<[Component[], number]>;
}
