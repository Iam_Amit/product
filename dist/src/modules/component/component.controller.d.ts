import { CommonService } from 'src/common/common.service';
import { ComponentService } from './component.service';
import { CreateComponentDto } from './dto/create-component.dto';
import { UpdateComponentDto } from './dto/update-component.dto';
import { Component } from './entities/component.entity';
import { Response } from 'express';
export declare class ComponentController {
    private readonly componentService;
    private readonly cs;
    constructor(componentService: ComponentService, cs: CommonService);
    create(createComponentDto: CreateComponentDto, req: any): Promise<Component>;
    findAll(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findOne(id: number): Promise<Component>;
    update(id: string, updateComponentDto: UpdateComponentDto, req: any): Promise<import("typeorm").UpdateResult>;
    remove(id: number, req: any): Promise<void>;
}
