export declare class Transaction {
    id: number;
    intent: string;
    state: string;
    cart: string;
    total: string;
    merchant_id: string;
}
