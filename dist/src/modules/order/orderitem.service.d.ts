import { ComponentRepository } from '../component/component.repository';
import { ProductRepository } from '../product/product.repository';
import { OrderItemDto } from './dto/create-orderItem.dto';
import { Order } from './entities/order.entity';
import { OrderItemRepository } from './orderItem.repository';
export declare class OrderItemService {
    private orderItemRepo;
    private productRepo;
    private componentRepo;
    constructor(orderItemRepo: OrderItemRepository, productRepo: ProductRepository, componentRepo: ComponentRepository);
    createItem(newOrder: Order, orderItem: OrderItemDto[]): Promise<any>;
}
