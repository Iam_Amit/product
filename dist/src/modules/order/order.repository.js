"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRepository = void 0;
const typeorm_1 = require("typeorm");
const order_entity_1 = require("./entities/order.entity");
let OrderRepository = class OrderRepository extends typeorm_1.Repository {
    async listOrder(search, page, limit) {
        const query = this.createQueryBuilder('order');
        query.skip((page >= 0 ? page : 0) * limit);
        query.limit(limit);
        if (search) {
            query.andWhere('order.fullName LIKE :search OR order.paymentMethod LIKE :search OR order.orderStatus LIKE :search OR order.paymentStatus LIKE :search OR order.phoneNumber LIKE :search', { search: `%${search}%` });
        }
        const orders = await query.getManyAndCount();
        return orders;
    }
};
OrderRepository = __decorate([
    (0, typeorm_1.EntityRepository)(order_entity_1.Order)
], OrderRepository);
exports.OrderRepository = OrderRepository;
//# sourceMappingURL=order.repository.js.map