"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.orderStatus = void 0;
var orderStatus;
(function (orderStatus) {
    orderStatus["Cancelled"] = "Cancelled";
    orderStatus["Delivered"] = "Delivered";
    orderStatus["InTransit"] = "InTransit";
    orderStatus["PaymentDue"] = "PaymentDue";
    orderStatus["PickupAvailable"] = "PickupAvailable";
    orderStatus["Problem"] = "Problem";
    orderStatus["Processing"] = "Processing";
    orderStatus["Returned"] = "Returned";
})(orderStatus = exports.orderStatus || (exports.orderStatus = {}));
//# sourceMappingURL=orderStatus.enum.js.map