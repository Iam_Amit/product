"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.paymentStatus = void 0;
var paymentStatus;
(function (paymentStatus) {
    paymentStatus["Pending"] = "Pending";
    paymentStatus["Completed"] = "Completed";
    paymentStatus["CREATED"] = "created";
    paymentStatus["CAPTURED"] = "captured";
    paymentStatus["DENIED"] = "denied";
    paymentStatus["EXPIRED"] = "expired";
    paymentStatus["PARTIALLY_CAPTURED"] = "partially_captured";
    paymentStatus["PARTIALLY_CREATED"] = "partially_created";
    paymentStatus["VOIDED"] = "voided";
    paymentStatus["PENDING"] = "pending";
})(paymentStatus = exports.paymentStatus || (exports.paymentStatus = {}));
//# sourceMappingURL=paymentStatus.enum.js.map