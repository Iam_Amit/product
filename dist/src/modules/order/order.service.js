"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const request = require("request");
const address_repository_1 = require("../address/address.repository");
const component_repository_1 = require("../component/component.repository");
const product_repository_1 = require("../product/product.repository");
const order_entity_1 = require("./entities/order.entity");
const order_repository_1 = require("./order.repository");
const orderItem_repository_1 = require("./orderItem.repository");
const orderitem_service_1 = require("./orderitem.service");
let OrderService = class OrderService {
    constructor(orderRepo, orderItemRepo, addressRepo, productRepo, componentRepo, oIs) {
        this.orderRepo = orderRepo;
        this.orderItemRepo = orderItemRepo;
        this.addressRepo = addressRepo;
        this.productRepo = productRepo;
        this.componentRepo = componentRepo;
        this.oIs = oIs;
        bn_Code: 'FLAVORsb-vano17750363_MP';
        const CLIENT = 'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav';
        const SECRET = 'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp';
        const PAYPAL_API = 'https://api-m.sandbox.paypal.com';
    }
    async getOrdersCount() {
        return await this.orderRepo.count();
    }
    async create(createOrderDto, user) {
        const { email, fullName, phoneNumber, items } = createOrderDto;
        let foundAddress = await this.addressRepo.findOne({
            id: createOrderDto.address,
        });
        if (!foundAddress) {
            throw new common_1.NotFoundException(` Address with id ${createOrderDto.address} not Exist !`);
        }
        const newOrder = new order_entity_1.Order();
        newOrder.address = foundAddress;
        newOrder.user = user;
        newOrder.email = email;
        newOrder.phoneNumber = phoneNumber;
        newOrder.fullName = fullName;
        newOrder.paymentMethod = 'CC';
        newOrder.paymentStatus = 'pending';
        newOrder.orderStatus = 'pending';
        let total = 0;
        items.forEach((x) => {
            total = x.quantity * x.price + total;
        });
        newOrder.total = total;
        try {
            await newOrder.save();
            let items = await this.oIs.createItem(newOrder, createOrderDto.items);
            items = items.map((x) => {
                delete x.order;
                return x;
            });
            return { order: newOrder, item: items };
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async findAll(page, limit, search) {
        let [output, searchTotal] = await this.orderRepo.listOrder(search, page, limit);
        let orders = await Promise.all(output.map(async (order) => {
            let items = await this.orderItemRepo.find({
                where: { order: order.id },
                relations: ['product', 'component'],
            });
            delete order.updatedAt;
            delete order.createdAt;
            delete order.deletedAt;
            return Object.assign(Object.assign({}, order), { items });
        }));
        const total = await this.orderRepo.count();
        return {
            results: orders,
            searchTotal,
            total,
        };
    }
    async findOne(id) {
        const order = await this.orderRepo.find({
            where: { id: id },
            relations: ['address', 'user'],
        });
        if (!order)
            throw new common_1.NotFoundException(`Order with the id ${id} not found`);
        const items = await this.orderItemRepo.find({
            where: { order: id },
            relations: ['product', 'component'],
        });
        return Object.assign(Object.assign({}, order[0]), { items });
    }
    update(id, updateOrderDto) {
        return this.orderRepo.update(id, updateOrderDto);
    }
    async remove(id) {
        const result = await this.orderRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`Order with the id ${id} not found`);
        }
    }
    createPayment(req, res) {
        request.post('https://api-m.sandbox.paypal.com' + '/v1/payments/payment', {
            auth: {
                user: 'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav',
                pass: 'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp',
            },
            body: {
                intent: 'sale',
                payer: {
                    payment_method: 'paypal',
                },
                transactions: [
                    {
                        amount: {
                            total: '5.99',
                            currency: 'USD',
                        },
                    },
                ],
                redirect_urls: {
                    return_url: 'https://example.com',
                    cancel_url: 'https://example.com',
                },
            },
            json: true,
        }, function (err, response) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            console.log('response response.body.id => ', response.body);
            return res.send({
                id: response.body.id,
            });
        });
    }
    executePayment(req, res) {
        console.log('req.body => ', req.body);
        console.log('paymentID => ', paymentID);
        console.log('payerID => ', payerID);
        var paymentID = req.body.paymentID;
        var payerID = req.body.payerID;
        request.post('https://api-m.sandbox.paypal.com' +
            '/v1/payments/payment/' +
            paymentID +
            '/execute', {
            auth: {
                user: 'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav',
                pass: 'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp',
            },
            body: {
                payer_id: payerID,
                transactions: [
                    {
                        amount: {
                            total: '10.99',
                            currency: 'USD',
                        },
                    },
                ],
            },
            json: true,
        }, function (err, response) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            res.json(response);
        });
    }
    paymentDetails(req, res, query) {
        request.get('https://api-m.sandbox.paypal.com/v1/payments/payment/' + query.id, {
            auth: {
                user: 'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav',
                pass: 'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp',
            },
            json: true,
        }, function (err, response) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            console.log('response response.body.id => ', response.body);
            return res.send({
                id: response.body,
            });
        });
    }
};
OrderService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(order_repository_1.OrderRepository)),
    __param(1, (0, typeorm_1.InjectRepository)(orderItem_repository_1.OrderItemRepository)),
    __param(2, (0, typeorm_1.InjectRepository)(address_repository_1.AddressRepository)),
    __param(3, (0, typeorm_1.InjectRepository)(product_repository_1.ProductRepository)),
    __param(4, (0, typeorm_1.InjectRepository)(component_repository_1.ComponentRepository)),
    __metadata("design:paramtypes", [order_repository_1.OrderRepository,
        order_repository_1.OrderRepository,
        address_repository_1.AddressRepository,
        product_repository_1.ProductRepository,
        component_repository_1.ComponentRepository,
        orderitem_service_1.OrderItemService])
], OrderService);
exports.OrderService = OrderService;
//# sourceMappingURL=order.service.js.map