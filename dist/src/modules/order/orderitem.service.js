"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderItemService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const component_repository_1 = require("../component/component.repository");
const product_repository_1 = require("../product/product.repository");
const orderItem_entity_1 = require("./entities/orderItem.entity");
const orderItem_repository_1 = require("./orderItem.repository");
let OrderItemService = class OrderItemService {
    constructor(orderItemRepo, productRepo, componentRepo) {
        this.orderItemRepo = orderItemRepo;
        this.productRepo = productRepo;
        this.componentRepo = componentRepo;
    }
    async createItem(newOrder, orderItem) {
        console.log('sadfnasjkldf');
        try {
            return await Promise.all(orderItem.map(async (element) => {
                console.log(element);
                let foundProduct;
                let foundComponent;
                if (element.product) {
                    try {
                        foundProduct = await this.productRepo.findOne(element.product);
                    }
                    catch (error) {
                        throw new common_1.InternalServerErrorException(error);
                    }
                }
                if (element.component) {
                    try {
                        foundComponent = await this.componentRepo.findOne(element.product);
                    }
                    catch (error) {
                        console.log(error);
                        throw new common_1.InternalServerErrorException(error);
                    }
                }
                console.log('component', foundComponent);
                console.log('product', foundProduct);
                const newItem = new orderItem_entity_1.OrderItem();
                newItem.order = newOrder;
                newItem.price = element.price;
                newItem.quantity = element.quantity;
                newItem.total = element.price * element.quantity;
                newItem.product = foundProduct;
                newItem.component = foundComponent;
                await newItem.save();
                return newItem;
            }));
        }
        catch (e) {
            console.log(e);
            return e;
        }
    }
};
OrderItemService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(orderItem_repository_1.OrderItemRepository)),
    __param(1, (0, typeorm_1.InjectRepository)(product_repository_1.ProductRepository)),
    __param(2, (0, typeorm_1.InjectRepository)(component_repository_1.ComponentRepository)),
    __metadata("design:paramtypes", [orderItem_repository_1.OrderItemRepository,
        product_repository_1.ProductRepository,
        component_repository_1.ComponentRepository])
], OrderItemService);
exports.OrderItemService = OrderItemService;
//# sourceMappingURL=orderitem.service.js.map