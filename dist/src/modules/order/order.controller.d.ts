import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { User } from '../users/entities/user.entity';
import { Response } from 'express';
import { Address } from '../address/entities/address.entity';
export declare class OrderController {
    private readonly orderService;
    constructor(orderService: OrderService);
    create(createOrderDto: CreateOrderDto, req: any): Promise<{
        order: import("./entities/order.entity").Order;
        item: import("./dto/create-orderItem.dto").OrderItemDto[];
    }>;
    findAll(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findOne(id: number): Promise<{
        items: import("./entities/order.entity").Order[];
        id: number;
        email: string;
        fullName: string;
        countryCode: string;
        phoneNumber: string;
        paymentMethod: string;
        paymentStatus: string;
        orderStatus: string;
        createdAt: Date;
        updatedAt: Date;
        deletedAt?: Date;
        total: number;
        user: User;
        address: Address;
        orderItems: import("./entities/orderItem.entity").OrderItem[];
    }>;
    update(id: string, updateOrderDto: UpdateOrderDto): Promise<import("typeorm").UpdateResult>;
    remove(id: string): Promise<void>;
    createPayment(req: any, res: Response): void;
    executePayment(req: any, res: Response): void;
    paymentDetails(req: any, res: Response, query: any): void;
}
