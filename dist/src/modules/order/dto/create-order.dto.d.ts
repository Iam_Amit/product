import { OrderItemDto } from './create-orderItem.dto';
export declare class CreateOrderDto {
    address: number;
    product: number;
    component: number;
    items: OrderItemDto[];
    fullName: string;
    email: string;
    phoneNumber: string;
    finalTotal: number;
}
