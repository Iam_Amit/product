import { AddressRepository } from '../address/address.repository';
import { ComponentRepository } from '../component/component.repository';
import { ProductRepository } from '../product/product.repository';
import { User } from '../users/entities/user.entity';
import { CreateOrderDto } from './dto/create-order.dto';
import { OrderItemDto } from './dto/create-orderItem.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { OrderRepository } from './order.repository';
import { OrderItemService } from './orderitem.service';
export declare class OrderService {
    private orderRepo;
    private orderItemRepo;
    private addressRepo;
    private productRepo;
    private componentRepo;
    private oIs;
    getOrdersCount(): Promise<number>;
    constructor(orderRepo: OrderRepository, orderItemRepo: OrderRepository, addressRepo: AddressRepository, productRepo: ProductRepository, componentRepo: ComponentRepository, oIs: OrderItemService);
    create(createOrderDto: CreateOrderDto, user: User): Promise<{
        order: Order;
        item: OrderItemDto[];
    }>;
    findAll(page: number, limit: number, search: string): Promise<{
        results: {
            items: Order[];
            id: number;
            email: string;
            fullName: string;
            countryCode: string;
            phoneNumber: string;
            paymentMethod: string;
            paymentStatus: string;
            orderStatus: string;
            createdAt: Date;
            updatedAt: Date;
            deletedAt?: Date;
            total: number;
            user: User;
            address: import("../address/entities/address.entity").Address;
            orderItems: import("./entities/orderItem.entity").OrderItem[];
        }[];
        searchTotal: number;
        total: number;
    }>;
    findOne(id: number): Promise<{
        items: Order[];
        id: number;
        email: string;
        fullName: string;
        countryCode: string;
        phoneNumber: string;
        paymentMethod: string;
        paymentStatus: string;
        orderStatus: string;
        createdAt: Date;
        updatedAt: Date;
        deletedAt?: Date;
        total: number;
        user: User;
        address: import("../address/entities/address.entity").Address;
        orderItems: import("./entities/orderItem.entity").OrderItem[];
    }>;
    update(id: number, updateOrderDto: UpdateOrderDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
    createPayment(req: any, res: any): void;
    executePayment(req: any, res: any): void;
    paymentDetails(req: any, res: any, query: any): void;
}
