import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
export declare class OrderRepository extends Repository<Order> {
    listOrder(search: string, page: number, limit: number): Promise<[Order[], number]>;
}
