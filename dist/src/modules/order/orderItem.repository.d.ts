import { Repository } from 'typeorm';
import { OrderItem } from './entities/orderItem.entity';
export declare class OrderItemRepository extends Repository<OrderItem> {
}
