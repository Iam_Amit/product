import { Component } from 'src/modules/component/entities/component.entity';
import { Product } from 'src/modules/product/entities/product.entity';
import { BaseEntity } from 'typeorm';
import { Order } from './order.entity';
export declare class OrderItem extends BaseEntity {
    id: number;
    quantity: number;
    price: number;
    total: number;
    order: Order;
    product: Product;
    component: Component;
}
