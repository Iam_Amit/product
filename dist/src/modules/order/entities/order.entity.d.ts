import { Address } from 'src/modules/address/entities/address.entity';
import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
import { OrderItem } from './orderItem.entity';
export declare class Order extends BaseEntity {
    id: number;
    email: string;
    fullName: string;
    countryCode: string;
    phoneNumber: string;
    paymentMethod: string;
    paymentStatus: string;
    orderStatus: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt?: Date;
    total: number;
    user: User;
    address: Address;
    orderItems: OrderItem[];
}
