import { PageService } from './page.service';
import { CreatePageDto } from './dto/create-page.dto';
import { UpdatePageDto } from './dto/update-page.dto';
import { Page } from './entities/page.entity';
import { CommonService } from 'src/common/common.service';
import { Response } from 'express';
export declare class PageController {
    private readonly pageService;
    private readonly cs;
    constructor(pageService: PageService, cs: CommonService);
    create(createPageDto: CreatePageDto, req: any): Promise<Page>;
    findAll(page: any, limit: any, res: Response, createPageDto: CreatePageDto, req: any): Promise<Response<any, Record<string, any>>>;
    findOne(id: number, req: any): Promise<Page>;
    update(id: string, updatePageDto: UpdatePageDto, req: any): Promise<import("typeorm").UpdateResult>;
    remove(id: number, req: any): Promise<void>;
}
