import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreatePageDto } from './dto/create-page.dto';
import { Page } from './entities/page.entity';
export declare class PageRepository extends Repository<Page> {
    createPage(createPageDto: CreatePageDto, user: User): Promise<Page>;
    listPage(createPageDto: CreatePageDto): Promise<Page[]>;
}
