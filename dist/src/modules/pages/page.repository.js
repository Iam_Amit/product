"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const page_entity_1 = require("./entities/page.entity");
let PageRepository = class PageRepository extends typeorm_1.Repository {
    async createPage(createPageDto, user) {
        const { links, title, description } = createPageDto;
        const page = new page_entity_1.Page();
        page.links = links;
        page.title = title;
        page.description = description;
        page.user = user;
        try {
            page.save();
            delete page.createdAt;
            delete page.deletedAt;
            delete page.updatedAt;
            return page;
        }
        catch (e) {
            throw new common_1.InternalServerErrorException('Error While saving Page');
        }
    }
    async listPage(createPageDto) {
        const query = this.createQueryBuilder('page');
        const page = await query.getMany();
        return page;
    }
};
PageRepository = __decorate([
    (0, typeorm_1.EntityRepository)(page_entity_1.Page)
], PageRepository);
exports.PageRepository = PageRepository;
//# sourceMappingURL=page.repository.js.map