import { User } from '../users/entities/user.entity';
import { CreatePageDto } from './dto/create-page.dto';
import { UpdatePageDto } from './dto/update-page.dto';
import { Page } from './entities/page.entity';
import { PageRepository } from './page.repository';
export declare class PageService {
    private pageRepo;
    getPageCount(): Promise<number>;
    constructor(pageRepo: PageRepository);
    create(createPageDto: CreatePageDto, user: User): Promise<Page>;
    findAll(page: any, limit: any, createPageDto: CreatePageDto): Promise<{
        results: Page[];
        total: number;
    }>;
    findOne(id: number): Promise<Page>;
    update(id: number, updatePageDto: UpdatePageDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
}
