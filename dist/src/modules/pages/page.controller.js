"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageController = void 0;
const common_1 = require("@nestjs/common");
const page_service_1 = require("./page.service");
const create_page_dto_1 = require("./dto/create-page.dto");
const update_page_dto_1 = require("./dto/update-page.dto");
const passport_1 = require("@nestjs/passport");
const common_service_1 = require("../../common/common.service");
let PageController = class PageController {
    constructor(pageService, cs) {
        this.pageService = pageService;
        this.cs = cs;
    }
    async create(createPageDto, req) {
        console.log(' User Role => ', req.user);
        const admin = await this.cs.checkRoleAdmin(req.user);
        console.log('admin => ', admin);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.pageService.create(createPageDto, req.user.id);
    }
    async findAll(page, limit, res, createPageDto, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        const val = await this.pageService.findAll(parseInt(page) - 1 || 0, parseInt(limit) || 10, createPageDto);
        return res.status(common_1.HttpStatus.OK).send(val);
    }
    async findOne(id, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.pageService.findOne(id);
    }
    async update(id, updatePageDto, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.pageService.update(+id, updatePageDto);
    }
    async remove(id, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.pageService.remove(id);
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_page_dto_1.CreatePageDto, Object]),
    __metadata("design:returntype", Promise)
], PageController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)('page')),
    __param(1, (0, common_1.Query)('limit')),
    __param(2, (0, common_1.Res)()),
    __param(4, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, create_page_dto_1.CreatePageDto, Object]),
    __metadata("design:returntype", Promise)
], PageController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], PageController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_page_dto_1.UpdatePageDto, Object]),
    __metadata("design:returntype", Promise)
], PageController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], PageController.prototype, "remove", null);
PageController = __decorate([
    (0, common_1.Controller)('page'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __metadata("design:paramtypes", [page_service_1.PageService,
        common_service_1.CommonService])
], PageController);
exports.PageController = PageController;
//# sourceMappingURL=page.controller.js.map