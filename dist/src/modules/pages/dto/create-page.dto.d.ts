export declare class CreatePageDto {
    links: string;
    title: string;
    description: string;
}
