import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
export declare class Page extends BaseEntity {
    id: number;
    links: string;
    title: string;
    description: string;
    deletedAt?: Date;
    createdAt: Date;
    updatedAt: Date;
    user: User;
}
