"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const page_repository_1 = require("./page.repository");
let PageService = class PageService {
    constructor(pageRepo) {
        this.pageRepo = pageRepo;
    }
    async getPageCount() {
        return await this.pageRepo.count();
    }
    create(createPageDto, user) {
        return this.pageRepo.createPage(createPageDto, user);
    }
    async findAll(page, limit, createPageDto) {
        const [output, total] = await this.pageRepo.findAndCount({
            take: limit,
            skip: (page >= 0 ? page : 0) * limit,
        });
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            return x;
        });
        return {
            results,
            total,
        };
    }
    async findOne(id) {
        const found = await this.pageRepo.findOne(id);
        if (!found) {
            throw new common_1.NotFoundException(`Page with the id ${id} not found`);
        }
        return found;
    }
    update(id, updatePageDto) {
        return this.pageRepo.update(id, updatePageDto);
    }
    async remove(id) {
        const result = await this.pageRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`Page  with the id ${id} not found`);
        }
    }
};
PageService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(page_repository_1.PageRepository)),
    __metadata("design:paramtypes", [page_repository_1.PageRepository])
], PageService);
exports.PageService = PageService;
//# sourceMappingURL=page.service.js.map