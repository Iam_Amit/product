"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const blog_entity_1 = require("./entities/blog.entity");
let BlogRepository = class BlogRepository extends typeorm_1.Repository {
    async createBlog(createBlogDto, user) {
        const { title, description, image, video, link } = createBlogDto;
        const blog = new blog_entity_1.Blog();
        blog.title = title;
        blog.description = description;
        blog.link = link;
        blog.image = image;
        blog.video = video;
        blog.user = user;
        try {
            await blog.save();
            delete blog.createdAt;
            delete blog.deletedAt;
            delete blog.updatedAt;
            return blog;
        }
        catch (e) {
            console.log(e);
            throw new common_1.InternalServerErrorException('Error While saving Blog');
        }
    }
    async listBlog(search, page, limit) {
        const query = this.createQueryBuilder('blog');
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('blog.title LIKE :search OR blog.description LIKE :search', { search: `%${search}%` });
        }
        const blogs = await query.getManyAndCount();
        console.log('blogs => ', blogs);
        return blogs;
    }
};
BlogRepository = __decorate([
    (0, typeorm_1.EntityRepository)(blog_entity_1.Blog)
], BlogRepository);
exports.BlogRepository = BlogRepository;
//# sourceMappingURL=blog.repository.js.map