import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
export declare class Blog extends BaseEntity {
    id: number;
    title: string;
    description: string;
    link: string;
    image: string;
    video: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt?: Date;
    user: User;
}
