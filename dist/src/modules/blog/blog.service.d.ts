import { User } from '../users/entities/user.entity';
import { BlogRepository } from './blog.repository';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { Blog } from './entities/blog.entity';
export declare class BlogService {
    private blogRepo;
    getBlogsCount(): Promise<number>;
    constructor(blogRepo: BlogRepository);
    create(createBlogDto: CreateBlogDto, user: User): Promise<Blog>;
    findAll(page: number, limit: number, search: string): Promise<{
        results: Blog[];
        searchTotal: number;
        total: number;
    }>;
    findOne(id: number): Promise<Blog>;
    update(id: number, updateBlogDto: UpdateBlogDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
}
