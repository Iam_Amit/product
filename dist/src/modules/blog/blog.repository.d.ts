import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateBlogDto } from './dto/create-blog.dto';
import { Blog } from './entities/blog.entity';
export declare class BlogRepository extends Repository<Blog> {
    createBlog(createBlogDto: CreateBlogDto, user: User): Promise<Blog>;
    listBlog(search: string, page: number, limit: number): Promise<[Blog[], number]>;
}
