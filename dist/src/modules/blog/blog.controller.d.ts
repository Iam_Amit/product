import { CommonService } from 'src/common/common.service';
import { Response } from 'express';
import { BlogService } from './blog.service';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { Blog } from './entities/blog.entity';
export declare class BlogController {
    private readonly blogService;
    private readonly cs;
    constructor(blogService: BlogService, cs: CommonService);
    create(createBlogDto: CreateBlogDto, req: any): Promise<Blog>;
    findAll(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findOne(id: number): Promise<Blog>;
    update(id: string, updateBlogDto: UpdateBlogDto, req: any): Promise<import("typeorm").UpdateResult>;
    remove(id: string, req: any): Promise<void>;
}
