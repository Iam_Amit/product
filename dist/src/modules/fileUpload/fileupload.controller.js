"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileUploadController = void 0;
const common_1 = require("@nestjs/common");
const common_service_1 = require("../../common/common.service");
const fileupload_service_1 = require("./fileupload.service");
let FileUploadController = class FileUploadController {
    constructor(fileUploadService, cs) {
        this.fileUploadService = fileUploadService;
        this.cs = cs;
    }
    async create(request, response) {
        try {
            await this.fileUploadService.fileupload(request, response);
        }
        catch (error) {
            return response
                .status(500)
                .json(`Failed to upload image file: ${error.message}`);
        }
    }
    async createVideoFile(request, response) {
        try {
            await this.fileUploadService.fileuploadVideo(request, response);
        }
        catch (error) {
            return response
                .status(500)
                .json(`Failed to upload image file: ${error.message}`);
        }
    }
};
__decorate([
    (0, common_1.Post)('upload'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FileUploadController.prototype, "create", null);
__decorate([
    (0, common_1.Post)('uploadVideo'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FileUploadController.prototype, "createVideoFile", null);
FileUploadController = __decorate([
    (0, common_1.Controller)('file'),
    __metadata("design:paramtypes", [fileupload_service_1.FileUploadService,
        common_service_1.CommonService])
], FileUploadController);
exports.FileUploadController = FileUploadController;
//# sourceMappingURL=fileupload.controller.js.map