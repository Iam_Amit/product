/// <reference types="qs" />
/// <reference types="express" />
export declare class FileUploadService {
    constructor();
    fileupload(req: any, res: any): Promise<any>;
    uploadImg: import("express").RequestHandler<import("express-serve-static-core").ParamsDictionary, any, any, import("qs").ParsedQs, Record<string, any>>;
    fileuploadVideo(req: any, res: any): Promise<any>;
    uploadVideo: import("express").RequestHandler<import("express-serve-static-core").ParamsDictionary, any, any, import("qs").ParsedQs, Record<string, any>>;
}
