"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileuploadModule = void 0;
const common_1 = require("@nestjs/common");
const aws_sdk_1 = require("aws-sdk");
const common_service_1 = require("../../common/common.service");
const fileupload_controller_1 = require("./fileupload.controller");
const fileupload_service_1 = require("./fileupload.service");
let FileuploadModule = class FileuploadModule {
};
FileuploadModule = __decorate([
    (0, common_1.Module)({
        imports: [],
        controllers: [fileupload_controller_1.FileUploadController],
        providers: [fileupload_service_1.FileUploadService, aws_sdk_1.ConfigService, common_service_1.CommonService],
        exports: [fileupload_service_1.FileUploadService],
    })
], FileuploadModule);
exports.FileuploadModule = FileuploadModule;
//# sourceMappingURL=fileupload.module.js.map