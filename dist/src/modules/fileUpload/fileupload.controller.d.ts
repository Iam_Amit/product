import { CommonService } from 'src/common/common.service';
import { FileUploadService } from './fileupload.service';
export declare class FileUploadController {
    private fileUploadService;
    private readonly cs;
    constructor(fileUploadService: FileUploadService, cs: CommonService);
    create(request: any, response: any): Promise<any>;
    createVideoFile(request: any, response: any): Promise<any>;
}
