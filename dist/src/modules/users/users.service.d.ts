import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { MailService } from 'src/mail/mail.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { EmailTokenDto } from './dto/email.token.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UserRepository } from './user.repository';
export declare class UsersService {
    private userRepo;
    private jwtService;
    private mailservice;
    private readonly configService;
    private twilioClient;
    private user;
    constructor(userRepo: UserRepository, jwtService: JwtService, mailservice: MailService, configService: ConfigService);
    create(createUserDto: CreateUserDto): Promise<void>;
    verifyEmail(emailTokenDto: EmailTokenDto): Promise<import("typeorm").UpdateResult>;
    singIn(createUserDto: CreateUserDto): Promise<{
        accessToken: string;
    }>;
    findAll(page: number, limit: number, search: string): Promise<{
        results: User[];
        searchTotal: number;
        total: number;
    }>;
    getAllUsersCount(): Promise<number>;
    findOne(id: number): Promise<User>;
    update(id: number, updateUserDto: UpdateUserDto): Promise<{
        message: string;
    }>;
    remove(id: number): Promise<void>;
    changePswd(changepswdDto: ChangePasswordDto, userId: any): Promise<import("typeorm").UpdateResult>;
    forgotPswd(emailTokenDto: EmailTokenDto): Promise<import("typeorm").UpdateResult>;
    resetPswd(resetPswdDto: ResetPasswordDto): Promise<import("typeorm").UpdateResult>;
    initiatePhoneNumberVerification(phoneNumber: string): Promise<string>;
    confirmPhoneNumber(userId: number, phoneNumber: string, verificationCode: string): Promise<void>;
}
