"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const common_1 = require("@nestjs/common");
const bcrypt = require("bcrypt");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./entities/user.entity");
const user_enum_1 = require("./entities/user.enum");
let UserRepository = class UserRepository extends typeorm_1.Repository {
    async createUser(createUserDto) {
        const { firstName, lastName, email, password, emailToken, isAdmin } = createUserDto;
        const user = new user_entity_1.User();
        user.firstName = firstName;
        user.lastName = lastName;
        user.email = email;
        user.emailToken = emailToken;
        user.role = isAdmin ? user_enum_1.UserRole.ADMIN : user_enum_1.UserRole.USER;
        let salt = await bcrypt.genSalt();
        user.password = await this.hashPassword(password, salt);
        try {
            await user.save();
        }
        catch (error) {
            console.log(error.code);
            if (error.code === '23505') {
                throw new common_1.ConflictException('Username already exist');
            }
            else {
                throw new common_1.InternalServerErrorException();
            }
        }
    }
    async listUser(search, page, limit) {
        const query = this.createQueryBuilder('users');
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('users.firstName LIKE :search OR users.lastName LIKE :search OR users.phoneNumber LIKE :search OR users.email LIKE :search', { search: `%${search}%` });
        }
        const users = await query.getManyAndCount();
        console.log('userss => ', users);
        return users;
    }
    async hashPassword(password, salt) {
        return bcrypt.hash(password, salt);
    }
    async validUserPassword(createUserDto) {
        const { email, password } = createUserDto;
        const user = await this.findOne({ email });
        console.log('User = > ', user);
        if (user && (await user.validatePassword(password))) {
            return user.email;
        }
        else {
            return null;
        }
    }
};
UserRepository = __decorate([
    (0, typeorm_1.EntityRepository)(user_entity_1.User)
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map