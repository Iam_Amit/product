"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const bcrypt = require("bcrypt");
const jwt_payload_interface_1 = require("../../jwt/jwt-payload-interface");
const mail_service_1 = require("../../mail/mail.service");
const twilio_1 = require("twilio");
const user_enum_1 = require("./entities/user.enum");
const user_repository_1 = require("./user.repository");
let UsersService = class UsersService {
    constructor(userRepo, jwtService, mailservice, configService) {
        this.userRepo = userRepo;
        this.jwtService = jwtService;
        this.mailservice = mailservice;
        this.configService = configService;
        const accountSid = configService.get('TWILIO_ACCOUNT_SID');
        const authToken = configService.get('TWILIO_AUTH_TOKEN');
        this.twilioClient = new twilio_1.Twilio(accountSid, authToken);
    }
    async create(createUserDto) {
        createUserDto.emailToken = await Math.floor(10000 + Math.random() * 90000).toString();
        const createUser = await this.userRepo.createUser(createUserDto);
        const data = await this.mailservice.sendUserConfirmation(createUserDto.emailToken, createUserDto.email, createUserDto.firstName);
        console.log('Data => ', data);
        return createUser;
    }
    async verifyEmail(emailTokenDto) {
        const email = emailTokenDto.email;
        const token = emailTokenDto.emailToken;
        console.log('Email => ', email);
        console.log('Email => ', token);
        const emailExist = await this.userRepo.findOne({
            email: email,
            emailToken: token,
        });
        console.log('Existed email = > ', emailExist);
        if (!emailExist) {
            throw new common_1.NotFoundException(`User  with the email ${email} not found`);
        }
        return this.userRepo.update({ email }, { emailToken: '', emailVerify: true });
    }
    async singIn(createUserDto) {
        console.log('qwerrr => ');
        const email = await this.userRepo.validUserPassword(createUserDto);
        console.log('SignIN => ', email);
        if (!email) {
            throw new common_1.UnauthorizedException('Invalid Credentials');
        }
        const payload = { email };
        const accessToken = await this.jwtService.sign(payload);
        return { accessToken };
    }
    async findAll(page, limit, search) {
        const [output, searchTotal] = await this.userRepo.listUser(search, page, limit);
        const total = await this.userRepo.count();
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            delete x.password;
            delete x.otp;
            delete x.role;
            delete x.emailToken;
            delete x.accessToken;
            return x;
        });
        return {
            results,
            searchTotal,
            total,
        };
    }
    async getAllUsersCount() {
        return await this.userRepo.count({ role: user_enum_1.UserRole.USER });
    }
    async findOne(id) {
        const found = await this.userRepo.findOne(id, {
            where: { id: id },
            relations: ['addresses', 'orders'],
        });
        if (!found) {
            throw new common_1.NotFoundException(`User  with the id ${id} not found`);
        }
        return found;
    }
    async update(id, updateUserDto) {
        await this.findOne(id);
        try {
            await this.userRepo.update(id, updateUserDto);
            return {
                message: 'User Updated',
            };
        }
        catch (e) {
            throw new common_1.InternalServerErrorException('Something went wrong');
        }
    }
    async remove(id) {
        const result = await this.userRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`User  with the id ${id} not found`);
        }
    }
    async changePswd(changepswdDto, userId) {
        const Oldpswd = await this.userRepo.findOne({});
        console.log('Old Pasword => ', Oldpswd.password);
        const oldPassword = changepswdDto.oldPassword;
        const newPassword = changepswdDto.newPassword;
        console.log('oldPassword => ', oldPassword);
        const match = await bcrypt.compare(oldPassword, oldPassword);
        const saltOrRounds = await bcrypt.genSalt();
        const newPswd = await this.userRepo.hashPassword(newPassword, saltOrRounds);
        console.log('New Paswd => ', newPswd);
        if (match) {
            console.log('Please enter the corredct OldPaswd ! ');
        }
        const userData = await this.userRepo.update({ id: userId }, { password: newPswd });
        return userData;
    }
    async forgotPswd(emailTokenDto) {
        const token = Math.floor(10000 + Math.random() * 90000).toString();
        const email = emailTokenDto.email;
        console.log('Email => ', email);
        const emailExist = await this.userRepo.findOne({ email });
        if (!emailExist) {
            throw new common_1.NotFoundException(`User  with the email ${email} not found`);
        }
        const data = await this.mailservice.sendUserConfirmation(token, emailTokenDto.email, emailTokenDto.firstName);
        console.log('Data => ', data);
        return this.userRepo.update({ email: emailExist.email }, { emailToken: token });
    }
    async resetPswd(resetPswdDto) {
        const token = resetPswdDto.emailToken;
        console.log('Email Token => ', token);
        const matchToken = await this.userRepo.findOne({ emailToken: token });
        console.log('Match Token => ', matchToken);
        if (!matchToken) {
            throw new common_1.NotFoundException(`Enter token ${token} do not match !`);
        }
        const updatePassword = resetPswdDto.updatePassword;
        console.log('update password => ', updatePassword);
        const saltOrRounds = await bcrypt.genSalt();
        const newPswd = await this.userRepo.hashPassword(updatePassword, saltOrRounds);
        console.log('New Paswd => ', newPswd);
        const userData = await this.userRepo.update({ emailToken: token }, { password: newPswd });
        return userData;
    }
    async initiatePhoneNumberVerification(phoneNumber) {
        const serviceSid = await this.configService.get('TWILIO_VERIFICATION_SERVICE_SID');
        console.log('serviceSid => ', serviceSid);
        const token = await Math.floor(10000 + Math.random() * 90000).toString();
        console.log('Singup API ', token);
        const sendToken = await this.twilioClient.messages
            .create({
            body: `Your one time OTP for verification is: ${token}`,
            from: '+18182736764',
            to: '+91' + phoneNumber,
        })
            .then((message) => console.log(message.sid));
        console.log('sendToken => ', sendToken);
        const otpToken = await this.userRepo.update({ phoneNumber: phoneNumber }, { otp: token });
        console.log('otpToken => ', otpToken);
        return 'Success !';
    }
    async confirmPhoneNumber(userId, phoneNumber, verificationCode) {
        const fndOtp = await this.userRepo.findOne({ id: userId });
        console.log('findOtp = > ', fndOtp.otp);
        if (fndOtp.otp !== verificationCode) {
            throw new common_1.BadRequestException('Wrong code provided');
        }
        await this.userRepo.update({ id: userId }, { isPhoneNumberConfirmed: true });
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        jwt_1.JwtService,
        mail_service_1.MailService,
        config_1.ConfigService])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map