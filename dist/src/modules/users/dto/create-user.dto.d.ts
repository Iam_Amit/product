export declare class CreateUserDto {
    firstName: string;
    lastName: string;
    password: string;
    email: string;
    emailToken: string;
    isAdmin: boolean;
}
