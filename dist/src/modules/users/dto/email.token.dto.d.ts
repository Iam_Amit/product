export declare class EmailTokenDto {
    email: string;
    emailToken: string;
    firstName: string;
    emailVerify: string;
}
