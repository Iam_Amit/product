export declare class ResetPasswordDto {
    emailToken: string;
    updatePassword: string;
}
