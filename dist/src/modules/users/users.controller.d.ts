import { Response } from 'express';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CheckVerificationCodeDto } from './dto/checkVerificationCode.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { EmailTokenDto } from './dto/email.token.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUserDto: CreateUserDto): Promise<void>;
    singIn(createUserDto: CreateUserDto): Promise<{
        accessToken: string;
    }>;
    findAll(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findOne(id: number): Promise<User>;
    update(id: string, updateUserDto: UpdateUserDto): Promise<{
        message: string;
    }>;
    remove(id: number): Promise<void>;
    changePswd(changepswdDto: ChangePasswordDto, request: any): Promise<import("typeorm").UpdateResult>;
    forgotPswd(emailTokenDto: EmailTokenDto): Promise<import("typeorm").UpdateResult>;
    resetPswd(resetPswdDto: ResetPasswordDto): Promise<import("typeorm").UpdateResult>;
    initiatePhoneNumberVerification(request: any): Promise<void>;
    checkVerificationCode(request: any, verificationData: CheckVerificationCodeDto): Promise<void>;
    verifyEmail(emailTokenDto: EmailTokenDto): Promise<void>;
}
