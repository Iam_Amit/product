import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
export declare class UserRepository extends Repository<User> {
    createUser(createUserDto: CreateUserDto): Promise<void>;
    listUser(search: string, page: number, limit: number): Promise<[User[], number]>;
    hashPassword(password: string, salt: string): Promise<string>;
    validUserPassword(createUserDto: CreateUserDto): Promise<string>;
}
