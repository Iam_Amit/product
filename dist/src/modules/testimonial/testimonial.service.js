"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestimonialService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const testimonial_repository_1 = require("./testimonial.repository");
let TestimonialService = class TestimonialService {
    constructor(testimonialRepo) {
        this.testimonialRepo = testimonialRepo;
    }
    create(createTestimonialDto, user) {
        return this.testimonialRepo.createTestimonial(createTestimonialDto, user);
    }
    async findAllUser(page, limit, search) {
        const [output, searchTotal] = await this.testimonialRepo.listTestimonialUser(search, page, limit);
        const total = await this.testimonialRepo.count();
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            return x;
        });
        return {
            results,
            searchTotal,
            total,
        };
    }
    async findAllAdmin(page, limit, search) {
        const [output, searchTotal] = await this.testimonialRepo.listTestimonialAdmin(search, page, limit);
        const total = await this.testimonialRepo.count();
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            return x;
        });
        return {
            results,
            searchTotal,
            total,
        };
    }
    async findOne(id, req) {
        const userName = req.user.fullname;
        console.log('UserName = > ', userName);
        const found = await this.testimonialRepo.findOne(id, {});
        if (!found) {
            throw new common_1.NotFoundException(`Testimonial  with the id ${id} not found`);
        }
        return found;
    }
    updateHideShow(id, hideShowTestDto) {
        console.log('qwertrew => ', typeof hideShowTestDto.isView);
        return this.testimonialRepo.update(id, hideShowTestDto);
    }
    update(id, updateTestimonialDto) {
        return this.testimonialRepo.update(id, updateTestimonialDto);
    }
    async remove(id) {
        const result = await this.testimonialRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`Testimonial  with the id ${id} not found`);
        }
    }
};
TestimonialService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(testimonial_repository_1.TestimonialRepository)),
    __metadata("design:paramtypes", [testimonial_repository_1.TestimonialRepository])
], TestimonialService);
exports.TestimonialService = TestimonialService;
//# sourceMappingURL=testimonial.service.js.map