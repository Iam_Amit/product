"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestimonialRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const testimonial_entity_1 = require("./entities/testimonial.entity");
let TestimonialRepository = class TestimonialRepository extends typeorm_1.Repository {
    async createTestimonial(createTestimonialDto, user) {
        const { title, description, rating } = createTestimonialDto;
        const testimonial = new testimonial_entity_1.Testimonial();
        testimonial.title = title;
        testimonial.description = description;
        testimonial.rating = rating;
        testimonial.user = user;
        try {
            await testimonial.save();
            delete testimonial.createdAt;
            delete testimonial.deletedAt;
            delete testimonial.updatedAt;
            return testimonial;
        }
        catch (e) {
            throw new common_1.InternalServerErrorException('Error While saving Testimonial');
        }
    }
    async listTestimonialUser(search, page, limit) {
        const query = this.createQueryBuilder('testimonial').where({
            isView: true,
        });
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('testimonial.title LIKE :search OR testimonial.description LIKE :search OR testimonial.rating LIKE :search', { search: `%${search}%` });
        }
        console.log(query.getSql());
        const testimonials = await query.getManyAndCount();
        console.log('testimonials => ', testimonials);
        return testimonials;
    }
    async listTestimonialAdmin(search, page, limit) {
        const query = this.createQueryBuilder('testimonial');
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('testimonial.title LIKE :search OR testimonial.description LIKE :search OR testimonial.rating LIKE :search', { search: `%${search}%` });
        }
        const testimonials = await query.getManyAndCount();
        console.log('testimonials => ', testimonials);
        return testimonials;
    }
};
TestimonialRepository = __decorate([
    (0, typeorm_1.EntityRepository)(testimonial_entity_1.Testimonial)
], TestimonialRepository);
exports.TestimonialRepository = TestimonialRepository;
//# sourceMappingURL=testimonial.repository.js.map