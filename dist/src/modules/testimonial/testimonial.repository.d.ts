import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateTestimonialDto } from './dto/create-testimonial.dto';
import { Testimonial } from './entities/testimonial.entity';
export declare class TestimonialRepository extends Repository<Testimonial> {
    createTestimonial(createTestimonialDto: CreateTestimonialDto, user: User): Promise<Testimonial>;
    listTestimonialUser(search: string, page: number, limit: number): Promise<[Testimonial[], number]>;
    listTestimonialAdmin(search: string, page: number, limit: number): Promise<[Testimonial[], number]>;
}
