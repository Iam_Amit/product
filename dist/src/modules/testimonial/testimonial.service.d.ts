import { User } from '../users/entities/user.entity';
import { CreateTestimonialDto } from './dto/create-testimonial.dto';
import { HideShowTestimonialDto } from './dto/update-hideSho-testimonial.dto';
import { UpdateTestimonialDto } from './dto/update-testimonial.dto';
import { Testimonial } from './entities/testimonial.entity';
import { TestimonialRepository } from './testimonial.repository';
export declare class TestimonialService {
    private testimonialRepo;
    constructor(testimonialRepo: TestimonialRepository);
    create(createTestimonialDto: CreateTestimonialDto, user: User): Promise<Testimonial>;
    findAllUser(page: number, limit: number, search: string): Promise<{
        results: Testimonial[];
        searchTotal: number;
        total: number;
    }>;
    findAllAdmin(page: number, limit: number, search: string): Promise<{
        results: Testimonial[];
        searchTotal: number;
        total: number;
    }>;
    findOne(id: number, req: any): Promise<Testimonial>;
    updateHideShow(id: number, hideShowTestDto: HideShowTestimonialDto): Promise<import("typeorm").UpdateResult>;
    update(id: number, updateTestimonialDto: UpdateTestimonialDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
}
