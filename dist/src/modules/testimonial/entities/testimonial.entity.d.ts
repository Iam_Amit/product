import { Component } from 'src/modules/component/entities/component.entity';
import { Product } from 'src/modules/product/entities/product.entity';
import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
export declare class Testimonial extends BaseEntity {
    id: number;
    title: string;
    description: string;
    rating: string;
    isView: boolean;
    deletedAt?: Date;
    createdAt: Date;
    updatedAt: Date;
    user: User;
    product: Product;
    component: Component;
}
