"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Testimonial = void 0;
const component_entity_1 = require("../../component/entities/component.entity");
const product_entity_1 = require("../../product/entities/product.entity");
const user_entity_1 = require("../../users/entities/user.entity");
const typeorm_1 = require("typeorm");
let Testimonial = class Testimonial extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], Testimonial.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Testimonial.prototype, "title", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Testimonial.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Testimonial.prototype, "rating", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true, type: 'boolean' }),
    __metadata("design:type", Boolean)
], Testimonial.prototype, "isView", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)(),
    __metadata("design:type", Date)
], Testimonial.prototype, "deletedAt", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Testimonial.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Testimonial.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)((type) => user_entity_1.User, (user) => user.testimonials),
    __metadata("design:type", user_entity_1.User)
], Testimonial.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)((type) => product_entity_1.Product, (product) => product.testimonials),
    __metadata("design:type", product_entity_1.Product)
], Testimonial.prototype, "product", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)((type) => component_entity_1.Component, (component) => component.testimonials),
    __metadata("design:type", component_entity_1.Component)
], Testimonial.prototype, "component", void 0);
Testimonial = __decorate([
    (0, typeorm_1.Entity)()
], Testimonial);
exports.Testimonial = Testimonial;
//# sourceMappingURL=testimonial.entity.js.map