"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestimonialRating = void 0;
var TestimonialRating;
(function (TestimonialRating) {
    TestimonialRating["zero"] = "0";
    TestimonialRating["one"] = "1";
    TestimonialRating["two"] = "2";
    TestimonialRating["three"] = "3";
    TestimonialRating["four"] = "4";
    TestimonialRating["five"] = "5";
})(TestimonialRating = exports.TestimonialRating || (exports.TestimonialRating = {}));
//# sourceMappingURL=testimonial.enum.js.map