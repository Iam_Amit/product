export declare class CreateTestimonialDto {
    title: string;
    description: string;
    rating: string;
}
