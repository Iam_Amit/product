import { TestimonialService } from './testimonial.service';
import { CreateTestimonialDto } from './dto/create-testimonial.dto';
import { UpdateTestimonialDto } from './dto/update-testimonial.dto';
import { Response } from 'express';
import { HideShowTestimonialDto } from './dto/update-hideSho-testimonial.dto';
export declare class TestimonialController {
    private readonly testimonialService;
    constructor(testimonialService: TestimonialService);
    create(createTestimonialDto: CreateTestimonialDto, req: any): Promise<import("./entities/testimonial.entity").Testimonial>;
    findAllUser(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findAllAdmin(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findOne(id: number, req: any): Promise<import("./entities/testimonial.entity").Testimonial>;
    updateHideShow(id: number, hideShowTestDto: HideShowTestimonialDto): Promise<import("typeorm").UpdateResult>;
    update(id: string, updateTestimonialDto: UpdateTestimonialDto): Promise<import("typeorm").UpdateResult>;
    remove(id: string): Promise<void>;
}
