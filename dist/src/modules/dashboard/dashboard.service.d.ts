import { BlogService } from '../blog/blog.service';
import { FaqService } from '../faq/faq.service';
import { OrderService } from '../order/order.service';
import { PageService } from '../pages/page.service';
import { ProductService } from '../product/product.service';
import { UsersService } from '../users/users.service';
export declare class DashboardService {
    private userService;
    private orderService;
    private productService;
    private blogService;
    private faqService;
    private pageService;
    constructor(userService: UsersService, orderService: OrderService, productService: ProductService, blogService: BlogService, faqService: FaqService, pageService: PageService);
    getAllStats(): Promise<{
        userCount: number;
        orderCount: number;
        productCount: number;
        blogCount: number;
        faqCount: number;
        pageCount: number;
    }>;
}
