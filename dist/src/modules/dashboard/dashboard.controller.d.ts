import { CommonService } from 'src/common/common.service';
import { User } from '../users/entities/user.entity';
import { DashboardService } from './dashboard.service';
export declare class DashboardController {
    private readonly dashboardService;
    private cs;
    constructor(dashboardService: DashboardService, cs: CommonService);
    getAllStats(user: User): Promise<{
        userCount: number;
        orderCount: number;
        productCount: number;
        blogCount: number;
        faqCount: number;
        pageCount: number;
    }>;
}
