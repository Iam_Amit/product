"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardService = void 0;
const common_1 = require("@nestjs/common");
const blog_service_1 = require("../blog/blog.service");
const faq_service_1 = require("../faq/faq.service");
const order_service_1 = require("../order/order.service");
const page_service_1 = require("../pages/page.service");
const product_service_1 = require("../product/product.service");
const users_service_1 = require("../users/users.service");
let DashboardService = class DashboardService {
    constructor(userService, orderService, productService, blogService, faqService, pageService) {
        this.userService = userService;
        this.orderService = orderService;
        this.productService = productService;
        this.blogService = blogService;
        this.faqService = faqService;
        this.pageService = pageService;
    }
    async getAllStats() {
        const userCount = await this.userService.getAllUsersCount();
        const orderCount = await this.orderService.getOrdersCount();
        const productCount = await this.productService.getproductsCount();
        const blogCount = await this.blogService.getBlogsCount();
        const faqCount = await this.faqService.getFaqCount();
        const pageCount = await this.pageService.getPageCount();
        return {
            userCount,
            orderCount,
            productCount,
            blogCount,
            faqCount,
            pageCount,
        };
    }
};
DashboardService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        order_service_1.OrderService,
        product_service_1.ProductService,
        blog_service_1.BlogService,
        faq_service_1.FaqService,
        page_service_1.PageService])
], DashboardService);
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard.service.js.map