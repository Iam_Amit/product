"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FaqService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const faq_repository_1 = require("./faq.repository");
let FaqService = class FaqService {
    constructor(faqRepo) {
        this.faqRepo = faqRepo;
    }
    async getFaqCount() {
        return await this.faqRepo.count();
    }
    create(createFaqDto, user) {
        return this.faqRepo.createFaq(createFaqDto, user);
    }
    async findAll(page, limit, search) {
        const [output, searchTotal] = await this.faqRepo.listFaq(search, page, limit);
        const total = await this.faqRepo.count();
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            return x;
        });
        return {
            results,
            searchTotal,
            total,
        };
    }
    async findOne(id) {
        const found = await this.faqRepo.findOne(id);
        if (!found) {
            throw new common_1.NotFoundException(`FAQ  with the id ${id} not found`);
        }
        return found;
    }
    update(id, updateFaqDto) {
        return this.faqRepo.update(id, updateFaqDto);
    }
    async remove(id) {
        const result = await this.faqRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`FAQ  with the id ${id} not found`);
        }
    }
};
FaqService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(faq_repository_1.FaqRepository)),
    __metadata("design:paramtypes", [faq_repository_1.FaqRepository])
], FaqService);
exports.FaqService = FaqService;
//# sourceMappingURL=faq.service.js.map