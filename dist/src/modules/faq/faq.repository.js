"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FaqRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const faq_entity_1 = require("./entities/faq.entity");
let FaqRepository = class FaqRepository extends typeorm_1.Repository {
    async createFaq(createFaqDto, user) {
        const { question, answer } = createFaqDto;
        const faq = new faq_entity_1.Faq();
        faq.question = question;
        faq.answer = answer;
        faq.user = user;
        try {
            await faq.save();
            delete faq.createdAt;
            delete faq.deletedAt;
            delete faq.updatedAt;
            return faq;
        }
        catch (e) {
            throw new common_1.InternalServerErrorException('Error While saving FAQ');
        }
    }
    async listFaq(search, page, limit) {
        const query = this.createQueryBuilder('faq');
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('faq.question LIKE :search OR faq.answer LIKE :search', {
                search: `%${search}%`,
            });
        }
        const faqs = await query.getManyAndCount();
        console.log('faqs => ', faqs);
        return faqs;
    }
};
FaqRepository = __decorate([
    (0, typeorm_1.EntityRepository)(faq_entity_1.Faq)
], FaqRepository);
exports.FaqRepository = FaqRepository;
//# sourceMappingURL=faq.repository.js.map