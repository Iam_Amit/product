import { Response } from 'express';
import { FaqService } from './faq.service';
import { CreateFaqDto } from './dto/create-faq.dto';
import { UpdateFaqDto } from './dto/update-faq.dto';
import { CommonService } from 'src/common/common.service';
export declare class FaqController {
    private readonly faqService;
    private readonly cs;
    constructor(faqService: FaqService, cs: CommonService);
    create(createFaqDto: CreateFaqDto, req: any): Promise<import("./entities/faq.entity").Faq>;
    findAll(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findOne(id: number): Promise<import("./entities/faq.entity").Faq>;
    update(id: string, updateFaqDto: UpdateFaqDto, req: any): Promise<import("typeorm").UpdateResult>;
    remove(id: string, req: any): Promise<void>;
}
