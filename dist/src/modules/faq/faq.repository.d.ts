import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateFaqDto } from './dto/create-faq.dto';
import { Faq } from './entities/faq.entity';
export declare class FaqRepository extends Repository<Faq> {
    createFaq(createFaqDto: CreateFaqDto, user: User): Promise<Faq>;
    listFaq(search: string, page: number, limit: number): Promise<[Faq[], number]>;
}
