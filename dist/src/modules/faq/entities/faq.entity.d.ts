import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
export declare class Faq extends BaseEntity {
    id: string;
    question: string;
    answer: string;
    deletedAt?: Date;
    createdAt: Date;
    updatedAt: Date;
    user: User;
}
