import { User } from '../users/entities/user.entity';
import { CreateFaqDto } from './dto/create-faq.dto';
import { UpdateFaqDto } from './dto/update-faq.dto';
import { Faq } from './entities/faq.entity';
import { FaqRepository } from './faq.repository';
export declare class FaqService {
    private faqRepo;
    getFaqCount(): Promise<number>;
    constructor(faqRepo: FaqRepository);
    create(createFaqDto: CreateFaqDto, user: User): Promise<Faq>;
    findAll(page: number, limit: number, search: string): Promise<{
        results: Faq[];
        searchTotal: number;
        total: number;
    }>;
    findOne(id: number): Promise<Faq>;
    update(id: number, updateFaqDto: UpdateFaqDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
}
