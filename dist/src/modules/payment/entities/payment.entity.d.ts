import { BaseEntity } from 'typeorm';
export declare class Payment extends BaseEntity {
    id: number;
    user_ID: string;
    product_ID: string;
    component_ID: string;
}
