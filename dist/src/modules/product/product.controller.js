"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductController = void 0;
const common_1 = require("@nestjs/common");
const product_service_1 = require("./product.service");
const create_product_dto_1 = require("./dto/create-product.dto");
const update_product_dto_1 = require("./dto/update-product.dto");
const passport_1 = require("@nestjs/passport");
const common_service_1 = require("../../common/common.service");
const hideShow_product_Dto_1 = require("./dto/hideShow-product.Dto");
let ProductController = class ProductController {
    constructor(productService, cs) {
        this.productService = productService;
        this.cs = cs;
    }
    async create(createProductDto, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        console.log('Role => ', req.user.role);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.productService.create(createProductDto);
    }
    async findAll(page, limit, search, res) {
        const val = await this.productService.findAll(parseInt(page) - 1 || 0, parseInt(limit) || 10, search);
        return res.status(common_1.HttpStatus.OK).send(val);
    }
    async findAllProdUser(page, limit, search, res) {
        const val = await this.productService.findAllProdUser(parseInt(page) - 1 || 0, parseInt(limit) || 10, search);
        return res.status(common_1.HttpStatus.OK).send(val);
    }
    findOne(id) {
        return this.productService.findOne(id);
    }
    findOneAdmin(id) {
        return this.productService.findOneAdmin(id);
    }
    async update(id, updateProductDto, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.productService.update(+id, updateProductDto);
    }
    async updateHideShow(id, hideShowTestDto, req) {
        return this.productService.updateHideShow(+id, hideShowTestDto);
    }
    async remove(id, req) {
        const admin = await this.cs.checkRoleAdmin(req.user);
        if (!admin)
            throw new common_1.ForbiddenException('Access Denied');
        return this.productService.remove(id);
    }
};
__decorate([
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_product_dto_1.CreateProductDto, Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "create", null);
__decorate([
    (0, common_1.Get)('Admin'),
    __param(0, (0, common_1.Query)('page')),
    __param(1, (0, common_1.Query)('limit')),
    __param(2, (0, common_1.Query)('search')),
    __param(3, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)('user'),
    __param(0, (0, common_1.Query)('page')),
    __param(1, (0, common_1.Query)('limit')),
    __param(2, (0, common_1.Query)('search')),
    __param(3, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "findAllProdUser", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "findOne", null);
__decorate([
    (0, common_1.Get)('admin/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "findOneAdmin", null);
__decorate([
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_product_dto_1.UpdateProductDto, Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "update", null);
__decorate([
    (0, common_1.Patch)('hide_view/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, hideShow_product_Dto_1.HideShowProductDto, Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "updateHideShow", null);
__decorate([
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "remove", null);
ProductController = __decorate([
    (0, common_1.Controller)('product'),
    __metadata("design:paramtypes", [product_service_1.ProductService,
        common_service_1.CommonService])
], ProductController);
exports.ProductController = ProductController;
//# sourceMappingURL=product.controller.js.map