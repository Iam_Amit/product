import { OrderItem } from 'src/modules/order/entities/orderItem.entity';
import { Testimonial } from 'src/modules/testimonial/entities/testimonial.entity';
import { BaseEntity } from 'typeorm';
export declare class Product extends BaseEntity {
    id: number;
    name: string;
    description: string;
    smallDescription: string;
    photo: string;
    video: string;
    price: string;
    quantity: string;
    isView: boolean;
    createdAt: Date;
    updatedAt: Date;
    deletedAt?: Date;
    orderItems: OrderItem[];
    testimonials: Testimonial[];
}
