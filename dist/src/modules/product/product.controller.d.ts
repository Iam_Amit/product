import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { CommonService } from 'src/common/common.service';
import { Response } from 'express';
import { HideShowProductDto } from './dto/hideShow-product.Dto';
export declare class ProductController {
    private readonly productService;
    private readonly cs;
    constructor(productService: ProductService, cs: CommonService);
    create(createProductDto: CreateProductDto, req: any): Promise<Product>;
    findAll(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findAllProdUser(page: any, limit: any, search: any, res: Response): Promise<Response<any, Record<string, any>>>;
    findOne(id: number): Promise<Product>;
    findOneAdmin(id: number): Promise<Product>;
    update(id: string, updateProductDto: UpdateProductDto, req: any): Promise<import("typeorm").UpdateResult>;
    updateHideShow(id: number, hideShowTestDto: HideShowProductDto, req: any): Promise<import("typeorm").UpdateResult>;
    remove(id: number, req: any): Promise<void>;
}
