import { CreateProductDto } from './dto/create-product.dto';
import { HideShowProductDto } from './dto/hideShow-product.Dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { ProductRepository } from './product.repository';
export declare class ProductService {
    private productRepo;
    getproductsCount(): Promise<number>;
    constructor(productRepo: ProductRepository);
    create(createProductDto: CreateProductDto): Promise<Product>;
    findAll(page: number, limit: number, search: string): Promise<{
        results: Product[];
        searchTotal: number;
        total: number;
    }>;
    findAllProdUser(page: number, limit: number, search: string): Promise<{
        results: Product[];
        searchTotal: number;
    }>;
    findOne(id: number): Promise<Product>;
    findOneAdmin(id: number): Promise<Product>;
    update(id: number, updateProductDto: UpdateProductDto): Promise<import("typeorm").UpdateResult>;
    updateHideShow(id: number, hideShowDto: HideShowProductDto): Promise<import("typeorm").UpdateResult>;
    remove(id: number): Promise<void>;
}
