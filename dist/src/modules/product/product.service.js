"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const product_repository_1 = require("./product.repository");
let ProductService = class ProductService {
    constructor(productRepo) {
        this.productRepo = productRepo;
    }
    async getproductsCount() {
        return await this.productRepo.count({ isView: true });
    }
    create(createProductDto) {
        return this.productRepo.createProduct(createProductDto);
    }
    async findAll(page, limit, search) {
        const [output, searchTotal] = await this.productRepo.listProductAdmin(search, page, limit);
        const total = await this.productRepo.count();
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            return x;
        });
        return {
            results,
            searchTotal,
            total,
        };
    }
    async findAllProdUser(page, limit, search) {
        const [output, searchTotal] = await this.productRepo.listProductUser(search, page, limit);
        const total = await this.productRepo.count();
        const results = output.map((x) => {
            delete x.updatedAt;
            delete x.createdAt;
            delete x.deletedAt;
            return x;
        });
        return {
            results,
            searchTotal,
        };
    }
    async findOne(id) {
        const found = await this.productRepo.findOne({ id, isView: true });
        if (!found) {
            throw new common_1.NotFoundException(`Product  with the id ${id} not found`);
        }
        return found;
    }
    async findOneAdmin(id) {
        const found = await this.productRepo.findOne({ id });
        if (!found) {
            throw new common_1.NotFoundException(`Product  with the id ${id} not found`);
        }
        return found;
    }
    update(id, updateProductDto) {
        return this.productRepo.update(id, updateProductDto);
    }
    async updateHideShow(id, hideShowDto) {
        const updateData = await this.productRepo.update(id, hideShowDto);
        console.log('updateData => ' + updateData);
        return updateData;
    }
    async remove(id) {
        const result = await this.productRepo.softDelete(id);
        if (result.affected === 0) {
            throw new common_1.NotFoundException(`Product with the id ${id} not found`);
        }
    }
};
ProductService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(product_repository_1.ProductRepository)),
    __metadata("design:paramtypes", [product_repository_1.ProductRepository])
], ProductService);
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map