"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const product_entity_1 = require("./entities/product.entity");
let ProductRepository = class ProductRepository extends typeorm_1.Repository {
    async createProduct(createProductDto) {
        const { name, description, smallDescription, price, photo, video, quantity, } = createProductDto;
        const product = new product_entity_1.Product();
        product.name = name;
        product.description = description;
        product.smallDescription = smallDescription;
        product.price = price;
        product.photo = photo;
        product.video = video;
        product.quantity = quantity;
        try {
            await product.save();
            delete product.createdAt;
            delete product.deletedAt;
            delete product.updatedAt;
            return product;
        }
        catch (e) {
            throw new common_1.InternalServerErrorException('Error While saving Product ');
        }
    }
    async listProductAdmin(search, page, limit) {
        const query = this.createQueryBuilder('product');
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('product.name LIKE :search OR product.description LIKE :search', { search: `%${search}%` });
        }
        const products = await query.getManyAndCount();
        console.log('products => ', products);
        return products;
    }
    async listProductUser(search, page, limit) {
        const query = this.createQueryBuilder('product').where({
            isView: true,
        });
        if (search) {
            query.skip((page >= 0 ? page : 0) * limit);
            query.limit(limit);
            query.andWhere('product.name LIKE :search OR product.description LIKE :search', { search: `%${search}%` });
        }
        console.log(query.getSql());
        const products = await query.getManyAndCount();
        return products;
    }
};
ProductRepository = __decorate([
    (0, typeorm_1.EntityRepository)(product_entity_1.Product)
], ProductRepository);
exports.ProductRepository = ProductRepository;
//# sourceMappingURL=product.repository.js.map