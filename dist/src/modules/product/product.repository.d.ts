import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { Product } from './entities/product.entity';
export declare class ProductRepository extends Repository<Product> {
    createProduct(createProductDto: CreateProductDto): Promise<Product>;
    listProductAdmin(search: string, page: number, limit: number): Promise<[Product[], number]>;
    listProductUser(search: string, page: number, limit: number): Promise<[Product[], number]>;
}
