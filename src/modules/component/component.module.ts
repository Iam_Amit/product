import { Module } from '@nestjs/common';
import { ComponentService } from './component.service';
import { ComponentController } from './component.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComponentRepository } from './component.repository';
import { ProductRepository } from '../product/product.repository';
import { CommonService } from 'src/common/common.service';

@Module({
  imports: [TypeOrmModule.forFeature([ComponentRepository, ProductRepository])],
  controllers: [ComponentController],
  providers: [ComponentService, CommonService],
})
export class ComponentModule {}
