import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';

import { CreateComponentDto } from './dto/create-component.dto';
import { Component } from './entities/component.entity';

@EntityRepository(Component)
export class ComponentRepository extends Repository<Component> {
  async createComponent(createComponentDto: CreateComponentDto) {
    const {
      name,
      description,
      smallDescription,
      price,
      photo,
      video,
      quantity,
    } = createComponentDto;

    const component = new Component();

    component.name = name;
    component.description = description;
    component.smallDescription = smallDescription;
    component.price = price;
    component.photo = photo;
    component.video = video;
    component.quantity = quantity;

    try {
      await component.save();

      delete component.createdAt;
      delete component.deletedAt;
      delete component.updatedAt;

      return component;
    } catch (e) {
      throw new InternalServerErrorException('Error While saving Component ');
    }
  }

  // Listing all component
  async listComponent(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('component');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'component.name LIKE :search OR component.description LIKE :search',
        { search: `%${search}%` },
      );
    }
    // console.log(query.getSql());
    const component = await query.getManyAndCount();
    console.log('component => ', component);
    return component;
  }
}
