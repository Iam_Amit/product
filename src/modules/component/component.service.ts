import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from '../product/entities/product.entity';
import { ProductRepository } from '../product/product.repository';
import { ComponentRepository } from './component.repository';
import { CreateComponentDto } from './dto/create-component.dto';
import { UpdateComponentDto } from './dto/update-component.dto';
import { Component } from './entities/component.entity';

@Injectable()
export class ComponentService {
  constructor(
    @InjectRepository(ComponentRepository)
    private componentRepo: ComponentRepository,
    @InjectRepository(ProductRepository)
    private productRepo: ProductRepository,
  ) {}

  // Creating Component
  async create(createComponentDto: CreateComponentDto) {
    return this.componentRepo.createComponent(createComponentDto);
  }

  // Listing all component
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.componentRepo.listComponent(
      search,
      page,
      limit,
    );
    const total = await this.componentRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // List component by Id
  async findOne(id: number): Promise<Component> {
    const found = await this.componentRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Component with the id ${id} not found`);
    }
    return found;
  }

  update(id: number, updateComponentDto: UpdateComponentDto) {
    return this.componentRepo.update(id, updateComponentDto);
  }

  // Removing the component
  async remove(id: number): Promise<void> {
    const result = await this.componentRepo.softDelete(id);
    // const result = await this.componentRepo.restore(id); In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`Component with the id ${id} not found`);
    }
  }
}
