// import { PartialType } from '@nestjs/mapped-types';
// import { CreateComponentDto } from './create-component.dto';

export class UpdateComponentDto {
  // id: number;

  name: string;

  description: string;

  photo: string;

  video: string;

  price: string;

  // quantity: number;

  // total: number;
}
