import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Req,
  UseGuards,
  ForbiddenException,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CommonService } from 'src/common/common.service';
import { Product } from '../product/entities/product.entity';
import { ComponentService } from './component.service';
import { CreateComponentDto } from './dto/create-component.dto';
import { UpdateComponentDto } from './dto/update-component.dto';
import { Component } from './entities/component.entity';
import { Response } from 'express';

@Controller('component')
export class ComponentController {
  constructor(
    private readonly componentService: ComponentService,
    private readonly cs: CommonService,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(@Body() createComponentDto: CreateComponentDto, @Req() req) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.componentService.create(createComponentDto);
  }

  // List the component
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.componentService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );

    return res.status(HttpStatus.OK).send(val);
  }

  // List component by Id
  @Get(':id')
  findOne(@Param('id') id: number): Promise<Component> {
    return this.componentService.findOne(id);
  }
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateComponentDto: UpdateComponentDto,
    @Req() req,
  ) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.componentService.update(+id, updateComponentDto);
  }

  // Removing component
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param('id') id: number, @Req() req): Promise<void> {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');

    return this.componentService.remove(id);
  }
}
