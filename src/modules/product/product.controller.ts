import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  Req,
  ForbiddenException,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { AuthGuard } from '@nestjs/passport';
import { CommonService } from 'src/common/common.service';
import { Response } from 'express';
import { HideShowProductDto } from './dto/hideShow-product.Dto';

@Controller('product')
// @UseGuards(AuthGuard('jwt'))
export class ProductController {
  constructor(
    private readonly productService: ProductService,
    private readonly cs: CommonService,
  ) {}

  // Creating the product
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(@Body() createProductDto: CreateProductDto, @Req() req) {
    const admin = await this.cs.checkRoleAdmin(req.user);
    console.log('Role => ', req.user.role);
    if (!admin) throw new ForbiddenException('Access Denied');
    return this.productService.create(createProductDto);
  }

  // Listing all product
  @Get('Admin')
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.productService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );

    return res.status(HttpStatus.OK).send(val);
  }
  // for User
  @Get('user')
  async findAllProdUser(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.productService.findAllProdUser(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );

    return res.status(HttpStatus.OK).send(val);
  }
  // Listing the product by Id for User
  @Get(':id')
  findOne(@Param('id') id: number): Promise<Product> {
    return this.productService.findOne(id);
  }
  // Listing the product by Id for Admin
  @Get('admin/:id')
  findOneAdmin(@Param('id') id: number): Promise<Product> {
    return this.productService.findOneAdmin(id);
  }
  // updating the product Details
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateProductDto: UpdateProductDto,
    @Req() req,
  ) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.productService.update(+id, updateProductDto);
  }
  // updating the hide show API
  @Patch('hide_view/:id')
  async updateHideShow(
    @Param('id') id: number,
    @Body() hideShowTestDto: HideShowProductDto,
    @Req() req,
  ) {
    // const admin = await this.cs.checkRoleAdmin(req.user);
    // if (!admin) throw new ForbiddenException('Access Denied');

    return this.productService.updateHideShow(+id, hideShowTestDto);
  }

  // Removing the product by Id
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param('id') id: number, @Req() req): Promise<void> {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.productService.remove(id);
  }
}
