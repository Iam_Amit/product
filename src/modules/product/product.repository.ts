import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { Product } from './entities/product.entity';

@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {
  async createProduct(createProductDto: CreateProductDto) {
    const {
      name,
      description,
      smallDescription,
      price,
      photo,
      video,
      quantity,
    } = createProductDto;

    const product = new Product();
    product.name = name;
    product.description = description;
    product.smallDescription = smallDescription;
    product.price = price;
    product.photo = photo;
    product.video = video;
    product.quantity = quantity;

    try {
      await product.save();
      delete product.createdAt;
      delete product.deletedAt;
      delete product.updatedAt;
      return product;
    } catch (e) {
      throw new InternalServerErrorException('Error While saving Product ');
    }
  }

  // Listing all the product
  async listProductAdmin(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('product');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'product.name LIKE :search OR product.description LIKE :search',
        { search: `%${search}%` },
      );
    }
    // console.log(query.getSql());
    const products = await query.getManyAndCount();
    console.log('products => ', products);
    return products;
  }

  // Listing all the product for User
  async listProductUser(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('product').where({
      isView: true,
    });
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'product.name LIKE :search OR product.description LIKE :search',
        { search: `%${search}%` },
      );
    }
    console.log(query.getSql());
    const products = await query.getManyAndCount();
    // console.log('products => ', products);
    return products;
  }
}
