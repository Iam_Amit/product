import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { HideShowProductDto } from './dto/hideShow-product.Dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { ProductRepository } from './product.repository';

@Injectable()
export class ProductService {
  //getting all product count
  async getproductsCount() {
    return await this.productRepo.count({ isView: true });
  }
  constructor(
    @InjectRepository(ProductRepository)
    private productRepo: ProductRepository,
  ) {}

  // Creating the product
  create(createProductDto: CreateProductDto) {
    return this.productRepo.createProduct(createProductDto);
  }

  // Listing all the product for Admin
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.productRepo.listProductAdmin(
      search,
      page,
      limit,
    );
    const total = await this.productRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Listing all the product for User
  async findAllProdUser(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.productRepo.listProductUser(
      search,
      page,
      limit,
    );
    const total = await this.productRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
    };
  }

  // List the Product by Id for User
  async findOne(id: number): Promise<Product> {
    const found = await this.productRepo.findOne({ id, isView: true });
    if (!found) {
      throw new NotFoundException(`Product  with the id ${id} not found`);
    }
    return found;
  }

  // List the product by id for Admin findOneAdmin
  async findOneAdmin(id: number): Promise<Product> {
    const found = await this.productRepo.findOne({ id });
    if (!found) {
      throw new NotFoundException(`Product  with the id ${id} not found`);
    }
    return found;
  }

  // Updating the product
  update(id: number, updateProductDto: UpdateProductDto) {
    return this.productRepo.update(id, updateProductDto);
  }
  // update Hide Show products
  async updateHideShow(id: number, hideShowDto: HideShowProductDto) {
    const updateData = await this.productRepo.update(id, hideShowDto);
    console.log('updateData => ' + updateData);
    return updateData;
  }

  // Remove the Product
  async remove(id: number): Promise<void> {
    const result = await this.productRepo.softDelete(id);
    // const result = await this.productRepo.restore(id); //In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`Product with the id ${id} not found`);
    }
  }
}
