export class CreateProductDto {
  id: number;

  name: string;

  description: string;

  smallDescription: string;

  photo: string;

  video: string;

  price: string;

  quantity: string;
}
