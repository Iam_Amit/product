import { PartialType } from '@nestjs/mapped-types';
import { CreateProductDto } from './create-product.dto';

export class UpdateProductDto extends PartialType(CreateProductDto) {
  name: string;

  description: string;

  photo: string;

  video: string;

  price: string;

  // quantity: number;

  // total: number;
}
