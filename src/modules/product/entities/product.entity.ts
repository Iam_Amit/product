import { type } from 'os';
import { Order } from 'src/modules/order/entities/order.entity';
import { OrderItem } from 'src/modules/order/entities/orderItem.entity';
import { Testimonial } from 'src/modules/testimonial/entities/testimonial.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Product extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  smallDescription: string;

  @Column({ nullable: true })
  photo: string;

  @Column({ nullable: true })
  video: string;

  @Column()
  price: string;

  @Column()
  quantity: string;

  @Column({ default: true, type: 'boolean' })
  isView: boolean;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @OneToMany((type) => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];

  @OneToMany((type) => Testimonial, (testimonial) => testimonial.product)
  testimonials: Testimonial[];
}
