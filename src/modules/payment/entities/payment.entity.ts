import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Payment extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_ID: string;

  @Column({ nullable: true })
  product_ID: string;

  @Column({ nullable: true })
  component_ID: string;
}
