import { IsNumberString, IsObject } from 'class-validator';
import { options } from 'joi';
import { IsNumeric } from 'sequelize-typescript';
import { OrderItemDto } from './create-orderItem.dto';

export class CreateOrderDto {
  address: number;

  product: number;

  component: number;

  items: OrderItemDto[];

  fullName: string;

  email: string;

  phoneNumber: string;

  finalTotal: number;
}
