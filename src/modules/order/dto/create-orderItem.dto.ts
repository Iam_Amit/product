export class OrderItemDto {
  product: number;

  component: number;

  quantity: number;

  total: number;

  price: number;

  order: number;
}
