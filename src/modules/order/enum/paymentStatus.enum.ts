export enum paymentStatus {
  Pending = 'Pending',
  Completed = 'Completed',
  CREATED = 'created',
  CAPTURED = 'captured',
  DENIED = 'denied',
  EXPIRED = 'expired',
  PARTIALLY_CAPTURED = 'partially_captured',
  PARTIALLY_CREATED = 'partially_created',
  VOIDED = 'voided',
  PENDING = 'pending',
}
