export enum orderStatus {
  Cancelled = 'Cancelled',
  Delivered = 'Delivered',
  InTransit = 'InTransit',
  PaymentDue = 'PaymentDue',
  PickupAvailable = 'PickupAvailable',
  Problem = 'Problem',
  Processing = 'Processing',
  Returned = 'Returned',
}
