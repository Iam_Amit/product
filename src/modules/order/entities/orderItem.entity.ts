import { Component } from 'src/modules/component/entities/component.entity';
import { Product } from 'src/modules/product/entities/product.entity';
import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './order.entity';

@Entity()
export class OrderItem extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  quantity: number;

  @Column()
  price: number;

  @Column()
  total: number;

  @ManyToOne((type) => Order, (order) => order.orderItems)
  order: Order;

  @ManyToOne((type) => Product, (product) => product.orderItems)
  product: Product;

  @ManyToOne((type) => Component, (component) => component.orderItems)
  component: Component;
}
