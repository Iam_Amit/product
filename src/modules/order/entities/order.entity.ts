import { IsEmail } from 'class-validator';
import { type } from 'os';
import { Address } from 'src/modules/address/entities/address.entity';
import { Component } from 'src/modules/component/entities/component.entity';
import { Product } from 'src/modules/product/entities/product.entity';
import { User } from 'src/modules/users/entities/user.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './orderItem.entity';

@Entity()
export class Order extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsEmail()
  email: string;

  @Column()
  fullName: string;

  @Column({ default: '+91' })
  countryCode: string;

  @Column()
  phoneNumber: string;

  @Column()
  paymentMethod: string;

  @Column()
  paymentStatus: string;

  @Column()
  orderStatus: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @Column({ nullable: true })
  total: number;

  @ManyToOne((type) => User, (user) => user.orders)
  user: User;

  @ManyToOne((type) => Address, (addres) => addres.orders)
  address: Address;

  @OneToMany((type) => OrderItem, (orderItems) => orderItems.order)
  orderItems: OrderItem[];
}
