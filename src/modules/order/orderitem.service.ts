import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ComponentRepository } from '../component/component.repository';
import { Component } from '../component/entities/component.entity';
import { Product } from '../product/entities/product.entity';
import { ProductRepository } from '../product/product.repository';
import { OrderItemDto } from './dto/create-orderItem.dto';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/orderItem.entity';
import { OrderItemRepository } from './orderItem.repository';

@Injectable()
export class OrderItemService {
  constructor(
    @InjectRepository(OrderItemRepository)
    private orderItemRepo: OrderItemRepository,
    @InjectRepository(ProductRepository)
    private productRepo: ProductRepository,
    @InjectRepository(ComponentRepository)
    private componentRepo: ComponentRepository,
  ) {}

  async createItem(newOrder: Order, orderItem: OrderItemDto[]) {
    console.log('sadfnasjkldf');
    try {
      return await Promise.all(
        orderItem.map(async (element) => {
          console.log(element);
          let foundProduct: Product;
          let foundComponent: Component;
          if (element.product) {
            try {
              foundProduct = await this.productRepo.findOne(element.product);
            } catch (error) {
              throw new InternalServerErrorException(error);
            }
          }

          if (element.component) {
            try {
              foundComponent = await this.componentRepo.findOne(
                element.product,
              );
            } catch (error) {
              console.log(error);
              throw new InternalServerErrorException(error);
            }
          }
          console.log('component', foundComponent);
          console.log('product', foundProduct);
          const newItem = new OrderItem();
          newItem.order = newOrder;
          newItem.price = element.price;
          newItem.quantity = element.quantity;
          newItem.total = element.price * element.quantity;
          newItem.product = foundProduct;
          newItem.component = foundComponent;
          await newItem.save();
          return newItem;
        }),
      );
    } catch (e) {
      console.log(e);
      return e;
    }
  }
}
