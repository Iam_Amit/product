import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as request from 'request';
import { AddressRepository } from '../address/address.repository';
import { ComponentRepository } from '../component/component.repository';
import { ProductRepository } from '../product/product.repository';
import { User } from '../users/entities/user.entity';
import { CreateOrderDto } from './dto/create-order.dto';
import { OrderItemDto } from './dto/create-orderItem.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { OrderRepository } from './order.repository';
import { OrderItemRepository } from './orderItem.repository';
import { OrderItemService } from './orderitem.service';

@Injectable()
export class OrderService {
  async getOrdersCount() {
    return await this.orderRepo.count();
  }
  constructor(
    @InjectRepository(OrderRepository)
    private orderRepo: OrderRepository,
    @InjectRepository(OrderItemRepository)
    private orderItemRepo: OrderRepository,
    @InjectRepository(AddressRepository)
    private addressRepo: AddressRepository,
    @InjectRepository(ProductRepository)
    private productRepo: ProductRepository,
    @InjectRepository(ComponentRepository)
    private componentRepo: ComponentRepository,
    private oIs: OrderItemService,
  ) {
    bn_Code: 'FLAVORsb-vano17750363_MP';
    const CLIENT =
      'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav';
    const SECRET =
      'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp';
    const PAYPAL_API = 'https://api-m.sandbox.paypal.com';
  }

  async create(createOrderDto: CreateOrderDto, user: User) {
    const { email, fullName, phoneNumber, items } = createOrderDto;

    let foundAddress = await this.addressRepo.findOne({
      id: createOrderDto.address,
    });
    if (!foundAddress) {
      throw new NotFoundException(
        ` Address with id ${createOrderDto.address} not Exist !`,
      );
    }
    const newOrder = new Order();
    newOrder.address = foundAddress;
    newOrder.user = user;
    newOrder.email = email;
    newOrder.phoneNumber = phoneNumber;
    newOrder.fullName = fullName;
    newOrder.paymentMethod = 'CC';
    newOrder.paymentStatus = 'pending';
    newOrder.orderStatus = 'pending';
    let total = 0;
    items.forEach((x) => {
      total = x.quantity * x.price + total;
    });
    newOrder.total = total;
    try {
      await newOrder.save();
      let items: OrderItemDto[] = await this.oIs.createItem(
        newOrder,
        createOrderDto.items,
      );
      items = items.map((x) => {
        delete x.order;
        return x;
      });
      return { order: newOrder, item: items };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  // finding all Order
  async findAll(page: number, limit: number, search: string) {
    let [output, searchTotal] = await this.orderRepo.listOrder(
      search,
      page,
      limit,
    );
    let orders = await Promise.all(
      output.map(async (order) => {
        let items = await this.orderItemRepo.find({
          where: { order: order.id },
          relations: ['product', 'component'],
        });
        delete order.updatedAt;
        delete order.createdAt;
        delete order.deletedAt;
        return { ...order, items };
      }),
    );
    const total = await this.orderRepo.count();
    return {
      results: orders,
      searchTotal,
      total,
    };

    // return this.orderRepo.listOrder(createOrderDto);
  }

  async findOne(id: number) {
    const order = await this.orderRepo.find({
      where: { id: id },
      relations: ['address', 'user'],
    });
    if (!order)
      throw new NotFoundException(`Order with the id ${id} not found`);
    const items = await this.orderItemRepo.find({
      where: { order: id },
      relations: ['product', 'component'],
    });
    return { ...order[0], items };
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return this.orderRepo.update(id, updateOrderDto);
  }

  async remove(id: number) {
    const result = await this.orderRepo.softDelete(id);
    // const result = await this.orderRepo.restore(id); //In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`Order with the id ${id} not found`);
    }
  }

  // Payment via PayPal
  createPayment(req, res) {
    request.post(
      'https://api-m.sandbox.paypal.com' + '/v1/payments/payment',
      {
        auth: {
          user: 'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav',
          pass: 'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp',
        },
        body: {
          intent: 'sale',
          payer: {
            payment_method: 'paypal',
          },
          transactions: [
            {
              amount: {
                total: '5.99',
                currency: 'USD',
              },
            },
          ],
          redirect_urls: {
            return_url: 'https://example.com',
            cancel_url: 'https://example.com',
          },
        },
        json: true,
      },
      function (err, response) {
        if (err) {
          console.error(err);
          return res.sendStatus(500);
        }
        console.log('response response.body.id => ', response.body);
        return res.send({
          id: response.body.id,
        });
      },
    );
  }

  // Execute payment
  executePayment(req, res) {
    console.log('req.body => ', req.body);
    console.log('paymentID => ', paymentID);
    console.log('payerID => ', payerID);
    // 2. Get the payment ID and the payer ID from the request body.
    var paymentID = req.body.paymentID;
    var payerID = req.body.payerID;
    // 3. Call /v1/payments/payment/PAY-XXX/execute to finalize the payment.
    request.post(
      'https://api-m.sandbox.paypal.com' +
        '/v1/payments/payment/' +
        paymentID +
        '/execute',
      {
        auth: {
          user: 'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav',
          pass: 'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp',
        },
        body: {
          payer_id: payerID,
          transactions: [
            {
              amount: {
                total: '10.99',
                currency: 'USD',
              },
            },
          ],
        },
        json: true,
      },
      function (err, response) {
        if (err) {
          console.error(err);
          return res.sendStatus(500);
        }
        // 4. Return a success response to the client
        res.json(response);
      },
    );
  }

  // get Payment Details
  paymentDetails(req, res, query) {
    request.get(
      'https://api-m.sandbox.paypal.com/v1/payments/payment/' + query.id,
      {
        auth: {
          user: 'AVxnWo3CJxzxcwcx9nYD1BG15FOoUIq_GMyLVF2CQ0tR477bQJfYVkoKpvV_9bZJUV7eT7O4hHMszRav',
          pass: 'EEkbNFpGQx6e5IvbjdKv6JlxC_fgrT5c0cgJyF1r7ptFqCC2n0idIPjilDIkd0FYxlqbVnpIyqZDOOWp',
        },
        json: true,
      },
      function (err, response) {
        if (err) {
          console.error(err);
          return res.sendStatus(500);
        }
        console.log('response response.body.id => ', response.body);
        return res.send({
          id: response.body,
        });
      },
    );
  }
}
