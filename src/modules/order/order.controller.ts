import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  Res,
  Query,
  HttpStatus,
} from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { User } from '../users/entities/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { query, Request, Response } from 'express';
import { Address } from '../address/entities/address.entity';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  // Generating order
  @Post()
  @UseGuards(AuthGuard('jwt'))
  create(@Body() createOrderDto: CreateOrderDto, @Req() req) {
    let user: User = req.user;
    return this.orderService.create(createOrderDto, user);
  }

  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.orderService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );
    return res.status(HttpStatus.OK).send(val);
    // return this.orderService.findAll(createOrderDto);
  }

  @Get('/d/:id')
  findOne(@Param('id') id: number) {
    return this.orderService.findOne(+id);
  }

  @Patch('/d/:id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.orderService.update(+id, updateOrderDto);
  }

  @Delete('/d/:id')
  remove(@Param('id') id: string) {
    return this.orderService.remove(+id);
  }

  //Set up Payment
  @Post('/my-api/create-payment/')
  createPayment(@Req() req, @Res() res: Response) {
    // console.log('req.user => ', req.user);
    return this.orderService.createPayment(req, res);
  }

  // Create Payment
  @Post('/my-api/execute-payment/')
  executePayment(@Req() req, @Res() res: Response) {
    // console.log('req.user => ', req.user);
    return this.orderService.executePayment(req, res);
  }

  // Get Payment Details
  @Get('/payment')
  paymentDetails(@Req() req, @Res() res: Response, @Query() query) {
    console.log('Request => ', query);
    return this.orderService.paymentDetails(req, res, query);
  }
}
