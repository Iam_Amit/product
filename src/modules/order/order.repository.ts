import { EntityRepository, Repository } from 'typeorm';
import { Order } from './entities/order.entity';

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
  // Listing all the product
  async listOrder(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('order');
    query.skip((page >= 0 ? page : 0) * limit);
    query.limit(limit);
    if (search) {
      query.andWhere(
        'order.fullName LIKE :search OR order.paymentMethod LIKE :search OR order.orderStatus LIKE :search OR order.paymentStatus LIKE :search OR order.phoneNumber LIKE :search',
        { search: `%${search}%` },
      );
    }
    const orders = await query.getManyAndCount();
    return orders;
  }
}
