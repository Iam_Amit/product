import { EntityRepository, Repository } from 'typeorm';
import { OrderItem } from './entities/orderItem.entity';

@EntityRepository(OrderItem)
export class OrderItemRepository extends Repository<OrderItem> {}
