import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderRepository } from './order.repository';
import { AddressRepository } from '../address/address.repository';
import { ComponentRepository } from '../component/component.repository';
import { ProductRepository } from '../product/product.repository';
import { OrderItemService } from './orderitem.service';
import { OrderItemRepository } from './orderItem.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OrderRepository,
      AddressRepository,
      ComponentRepository,
      ProductRepository,
      OrderItemRepository,
    ]),
  ],
  controllers: [OrderController],
  providers: [OrderService, OrderItemService],
  exports: [OrderService],
})
export class OrderModule {}
