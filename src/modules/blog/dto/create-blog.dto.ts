export class CreateBlogDto {
  title: string;

  description: string;

  link: string;

  image: string;

  video: string;
}
