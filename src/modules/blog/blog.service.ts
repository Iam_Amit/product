import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { BlogRepository } from './blog.repository';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { Blog } from './entities/blog.entity';

@Injectable()
export class BlogService {
  async getBlogsCount() {
    return await this.blogRepo.count();
  }
  constructor(
    @InjectRepository(BlogRepository)
    private blogRepo: BlogRepository,
  ) {}

  // Creating the blog
  create(createBlogDto: CreateBlogDto, user: User) {
    return this.blogRepo.createBlog(createBlogDto, user);
  }

  // Listing all blog
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.blogRepo.listBlog(
      search,
      page,
      limit,
    );
    const total = await this.blogRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Listing the blog by Id
  async findOne(id: number): Promise<Blog> {
    const found = await this.blogRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Blog with the id ${id} not found`);
    }
    return found;
  }

  update(id: number, updateBlogDto: UpdateBlogDto) {
    return this.blogRepo.update(id, updateBlogDto);
  }

  // Removing the blog
  async remove(id: number): Promise<void> {
    const result = await this.blogRepo.softDelete(id);
    // const result = await this.blogRepo.restore(id); In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`Blog with the id ${id} not found`);
    }
  }
}
