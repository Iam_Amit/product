import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  Req,
  ForbiddenException,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CommonService } from 'src/common/common.service';
import { Response } from 'express';
import { BlogService } from './blog.service';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { Blog } from './entities/blog.entity';

@Controller('blog')
export class BlogController {
  constructor(
    private readonly blogService: BlogService,
    private readonly cs: CommonService,
  ) {}

  //Creating the blog
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(@Body() createBlogDto: CreateBlogDto, @Req() req) {
    console.log('User => ', req.user.id);

    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.blogService.create(createBlogDto, req.user.id);
  }

  // Listing all Blog
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.blogService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );
    return res.status(HttpStatus.OK).send(val);
    // return this.blogService.findAll(createBlogDto);
  }

  // Listing the blog By Id
  @Get(':id')
  findOne(@Param('id') id: number): Promise<Blog> {
    return this.blogService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  async update(
    @Param('id') id: string,
    @Body() updateBlogDto: UpdateBlogDto,
    @Req() req,
  ) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.blogService.update(+id, updateBlogDto);
  }

  // Removing the blog
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param('id') id: string, @Req() req) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.blogService.remove(+id);
  }
}
