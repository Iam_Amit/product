import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateBlogDto } from './dto/create-blog.dto';
import { Blog } from './entities/blog.entity';

@EntityRepository(Blog)
export class BlogRepository extends Repository<Blog> {
  async createBlog(createBlogDto: CreateBlogDto, user: User) {
    const { title, description, image, video, link } = createBlogDto;

    const blog = new Blog();
    blog.title = title;
    blog.description = description;
    blog.link = link;
    blog.image = image;
    blog.video = video;
    blog.user = user;

    try {
      await blog.save();
      delete blog.createdAt;
      delete blog.deletedAt;
      delete blog.updatedAt;
      return blog;
    } catch (e) {
      console.log(e);
      throw new InternalServerErrorException('Error While saving Blog');
    }
  }

  // Listing all the blog
  async listBlog(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('blog');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'blog.title LIKE :search OR blog.description LIKE :search',
        { search: `%${search}%` },
      );
    }
    // console.log(query.getSql());
    const blogs = await query.getManyAndCount();
    console.log('blogs => ', blogs);
    return blogs;
  }
}
