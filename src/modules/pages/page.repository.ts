import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreatePageDto } from './dto/create-page.dto';
import { Page } from './entities/page.entity';

@EntityRepository(Page)
export class PageRepository extends Repository<Page> {
  // Creating the resource
  async createPage(createPageDto: CreatePageDto, user: User): Promise<Page> {
    const { links, title, description } = createPageDto;

    const page = new Page();
    page.links = links;
    page.title = title;
    page.description = description;
    page.user = user;

    try {
      page.save();
      delete page.createdAt;
      delete page.deletedAt;
      delete page.updatedAt;
      return page;
    } catch (e) {
      throw new InternalServerErrorException('Error While saving Page');
    }
  }

  // Listing the Page
  async listPage(createPageDto: CreatePageDto): Promise<Page[]> {
    const query = this.createQueryBuilder('page');
    const page = await query.getMany();
    return page;
  }
}
