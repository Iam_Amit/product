import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { CreatePageDto } from './dto/create-page.dto';
import { UpdatePageDto } from './dto/update-page.dto';
import { Page } from './entities/page.entity';
import { PageRepository } from './page.repository';

@Injectable()
export class PageService {
  async getPageCount() {
    return await this.pageRepo.count();
  }

  constructor(
    @InjectRepository(PageRepository)
    private pageRepo: PageRepository,
  ) {}

  create(createPageDto: CreatePageDto, user: User) {
    return this.pageRepo.createPage(createPageDto, user);
  }

  // Listing  pages
  async findAll(page, limit, createPageDto: CreatePageDto) {
    const [output, total] = await this.pageRepo.findAndCount({
      take: limit,
      skip: (page >= 0 ? page : 0) * limit,
    });
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      total,
    };

    // return this.pageRepo.listPage(createPageDto);
  }

  // List page via ID
  async findOne(id: number): Promise<Page> {
    const found = await this.pageRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Page with the id ${id} not found`);
    }
    return found;
  }

  update(id: number, updatePageDto: UpdatePageDto) {
    return this.pageRepo.update(id, updatePageDto);
  }

  // removing the Page
  async remove(id: number): Promise<void> {
    const result = await this.pageRepo.softDelete(id);
    // const result = await this.PageRepo.restore(id); //In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`Page  with the id ${id} not found`);
    }
  }
}
