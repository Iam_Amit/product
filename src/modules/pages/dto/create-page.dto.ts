export class CreatePageDto {
  links: string;

  title: string;

  description: string;
}
