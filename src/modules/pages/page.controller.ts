import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  ForbiddenException,
  Query,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { PageService } from './page.service';
import { CreatePageDto } from './dto/create-page.dto';
import { UpdatePageDto } from './dto/update-page.dto';
import { Page } from './entities/page.entity';
import { AuthGuard } from '@nestjs/passport';
import { CommonService } from 'src/common/common.service';
import { Response } from 'express';

@Controller('page')
@UseGuards(AuthGuard('jwt'))
export class PageController {
  constructor(
    private readonly pageService: PageService,
    private readonly cs: CommonService,
  ) {}

  // Creating the pages
  @Post()
  async create(@Body() createPageDto: CreatePageDto, @Req() req) {
    console.log(' User Role => ', req.user);

    const admin = await this.cs.checkRoleAdmin(req.user);
    console.log('admin => ', admin);

    if (!admin) throw new ForbiddenException('Access Denied');

    return this.pageService.create(createPageDto, req.user.id);
  }

  // List all
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Res() res: Response,
    createPageDto: CreatePageDto,
    @Req() req,
  ) {
    const admin = await this.cs.checkRoleAdmin(req.user);
    if (!admin) throw new ForbiddenException('Access Denied');
    const val = await this.pageService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      createPageDto,
    );

    return res.status(HttpStatus.OK).send(val);
    // return this.pageService.findAll(createPageDto);
  }

  // List by Id
  @Get(':id')
  async findOne(@Param('id') id: number, @Req() req) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.pageService.findOne(id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePageDto: UpdatePageDto,
    @Req() req,
  ) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.pageService.update(+id, updatePageDto);
  }

  // Remove
  @Delete(':id')
  async remove(@Param('id') id: number, @Req() req): Promise<void> {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.pageService.remove(id);
  }
}
