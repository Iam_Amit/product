import { Module } from '@nestjs/common';
import { PageService } from './page.service';
import { PageController } from './page.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PageRepository } from './page.repository';
import { CommonService } from 'src/common/common.service';

@Module({
  imports: [TypeOrmModule.forFeature([PageRepository])],
  controllers: [PageController],
  providers: [PageService, CommonService],
  exports: [PageService],
})
export class PageModule {}
