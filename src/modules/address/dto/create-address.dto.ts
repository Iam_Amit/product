export class CreateAddressDto {
  id: number;

  location: string;

  city: string;

  state: string;

  streetNo: string;

  latitude: string;

  longitude: string;

  zipCode: number;
}
