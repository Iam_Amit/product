import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AddressService } from './address.service';
import { CreateAddressDto } from './dto/create-address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';
import { Address } from './entities/address.entity';

@Controller('address')
@UseGuards(AuthGuard('jwt'))
export class AddressController {
  constructor(private readonly addressService: AddressService) {}

  // Creating the Address
  @Post()
  create(@Body() createAddressDto: CreateAddressDto, @Req() req) {
    console.log('Requested Data = > ', req.user.id);
    return this.addressService.create(createAddressDto, req.user.id);
  }

  // Get Users Address
  @Get()
  findAll(@Req() req): Promise<Address[]> {
    return this.addressService.findAll(req.user.id);
  }

  // Address by id
  @Get(':id')
  findOne(@Param('id') id: number): Promise<Address> {
    return this.addressService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAddressDto: UpdateAddressDto) {
    return this.addressService.update(+id, updateAddressDto);
  }

  // Remove Address
  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.addressService.remove(id);
  }
}
