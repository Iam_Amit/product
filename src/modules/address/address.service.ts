import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { AddressRepository } from './address.repository';
import { CreateAddressDto } from './dto/create-address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';
import { Address } from './entities/address.entity';

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(AddressRepository)
    private addressRepo: AddressRepository,
  ) {}

  // Creating addresss
  create(createAddressDto: CreateAddressDto, user: User) {
    return this.addressRepo.createAddress(createAddressDto, user);
  }

  // Get All Address
  findAll(user: User): Promise<Address[]> {
    return this.addressRepo.find({ where: { user: user } });
  }

  // Find address by id
  async findOne(id: number): Promise<Address> {
    const found = await this.addressRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Address with the id ${id} not found`);
    }
    return found;
  }

  update(id: number, updateAddressDto: UpdateAddressDto) {
    return this.addressRepo.update(id, updateAddressDto);
  }

  // Remove the Address
  async remove(id: number): Promise<void> {
    const result = await this.addressRepo.softDelete(id);
    // const result = await this.componentRepo.restore(id); In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`Address with the id ${id} not found`);
    }
  }
}
