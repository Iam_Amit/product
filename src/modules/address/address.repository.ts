import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateAddressDto } from './dto/create-address.dto';
import { Address } from './entities/address.entity';

@EntityRepository(Address)
export class AddressRepository extends Repository<Address> {
  async createAddress(
    createAddressDto: CreateAddressDto,
    user: User,
  ): Promise<Address> {
    const {
      location,
      city,
      state,
      streetNo,
      latitude,
      longitude,
      zipCode,
      // userId,
    } = createAddressDto;

    const address = new Address();
    address.address = location;
    address.city = city;
    address.state = state;
    address.streetNo = streetNo;
    address.latitude = latitude;
    address.longitude = longitude;
    address.zipCode = zipCode;
    address.user = user;
    try {
      await address.save();
      delete address.updatedAt;
      delete address.deletedAt;
      return address;
    } catch (e) {
      console.log(e);
      throw new InternalServerErrorException('Error While saving Address');
    }
  }

  // Listing all Address
  async listAddress(): Promise<Address[]> {
    const query = this.createQueryBuilder('address');
    const address = await query.getMany();
    return address;
  }
}
