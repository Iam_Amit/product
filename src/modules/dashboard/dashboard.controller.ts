import { Controller, ForbiddenException, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/decorator/getuser.decorator';
import { CommonService } from 'src/common/common.service';
import { User } from '../users/entities/user.entity';
import { DashboardService } from './dashboard.service';

@Controller('dashboard')
export class DashboardController {
  constructor(
    private readonly dashboardService: DashboardService,
    private cs: CommonService,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('dash')
  async getAllStats(@GetUser() user: User) {
    const admin = this.cs.checkRoleAdmin(user);
    if (!admin) throw new ForbiddenException('Access Denied');
    return await this.dashboardService.getAllStats();
  }
}
