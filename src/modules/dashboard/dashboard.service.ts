import { Injectable } from '@nestjs/common';
import { BlogService } from '../blog/blog.service';
import { FaqService } from '../faq/faq.service';
import { OrderService } from '../order/order.service';
import { PageService } from '../pages/page.service';
import { ProductService } from '../product/product.service';
import { UsersService } from '../users/users.service';

@Injectable()
export class DashboardService {
  constructor(
    private userService: UsersService,
    private orderService: OrderService,
    private productService: ProductService,
    private blogService: BlogService,
    private faqService: FaqService,
    private pageService: PageService,
  ) {}

  async getAllStats() {
    const userCount = await this.userService.getAllUsersCount();
    const orderCount = await this.orderService.getOrdersCount();
    const productCount = await this.productService.getproductsCount();
    const blogCount = await this.blogService.getBlogsCount();
    const faqCount = await this.faqService.getFaqCount();
    const pageCount = await this.pageService.getPageCount();

    return {
      userCount,
      orderCount,
      productCount,
      blogCount,
      faqCount,
      pageCount,
    };
  }
}
