import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from 'src/common/common.module';
import { BlogModule } from '../blog/blog.module';
import { FaqModule } from '../faq/faq.module';
import { OrderModule } from '../order/order.module';
import { PageModule } from '../pages/page.module';
import { ProductModule } from '../product/product.module';
import { UserRepository } from '../users/user.repository';
import { UsersModule } from '../users/users.module';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';

@Module({
  imports: [
    UsersModule,
    OrderModule,
    ProductModule,
    BlogModule,
    FaqModule,
    PageModule,
    CommonModule,
    TypeOrmModule.forFeature([UserRepository]),
  ],
  controllers: [DashboardController],
  providers: [DashboardService],
})
export class DashboardModule {}
