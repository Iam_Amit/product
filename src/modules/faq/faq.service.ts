import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { CreateFaqDto } from './dto/create-faq.dto';
import { UpdateFaqDto } from './dto/update-faq.dto';
import { Faq } from './entities/faq.entity';
import { FaqRepository } from './faq.repository';

@Injectable()
export class FaqService {
  async getFaqCount() {
    return await this.faqRepo.count();
  }

  constructor(
    @InjectRepository(FaqRepository)
    private faqRepo: FaqRepository,
  ) {}

  // creaing the FAQ
  create(createFaqDto: CreateFaqDto, user: User) {
    return this.faqRepo.createFaq(createFaqDto, user);
  }

  // Listing all FAQ
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.faqRepo.listFaq(
      search,
      page,
      limit,
    );
    const total = await this.faqRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Listing the FAQ by Id
  async findOne(id: number): Promise<Faq> {
    const found = await this.faqRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`FAQ  with the id ${id} not found`);
    }
    return found;
  }

  update(id: number, updateFaqDto: UpdateFaqDto) {
    return this.faqRepo.update(id, updateFaqDto);
  }

  // Removing the FAQ
  async remove(id: number): Promise<void> {
    const result = await this.faqRepo.softDelete(id);
    // const result = await this.faqRepo.restore(id); //In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`FAQ  with the id ${id} not found`);
    }
  }
}
