import { Module } from '@nestjs/common';
import { FaqService } from './faq.service';
import { FaqController } from './faq.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FaqRepository } from './faq.repository';
import { CommonService } from 'src/common/common.service';

@Module({
  imports: [TypeOrmModule.forFeature([FaqRepository])],
  controllers: [FaqController],
  providers: [FaqService, CommonService],
  exports: [FaqService],
})
export class FaqModule {}
