import { Response } from 'express';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  UseGuards,
  Req,
  ForbiddenException,
  Query,
  HttpStatus,
} from '@nestjs/common';
import { FaqService } from './faq.service';
import { CreateFaqDto } from './dto/create-faq.dto';
import { UpdateFaqDto } from './dto/update-faq.dto';
import { AuthGuard } from '@nestjs/passport';
import { CommonService } from 'src/common/common.service';

@Controller('faq')
export class FaqController {
  constructor(
    private readonly faqService: FaqService,
    private readonly cs: CommonService,
  ) {}

  // Creating the FAQ
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(@Body() createFaqDto: CreateFaqDto, @Req() req) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');

    return this.faqService.create(createFaqDto, req.user.id);
  }

  // List All FAQ
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.faqService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );
    // return this.faqService.findAll(createFaqDto);
    return res.status(HttpStatus.OK).send(val);
  }

  // List the FAQ By Id
  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.faqService.findOne(id);
  }

  // Updating the Data
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateFaqDto: UpdateFaqDto,
    @Req() req,
  ) {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.faqService.update(+id, updateFaqDto);
  }

  // Remove the FAQ
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async remove(@Param('id') id: string, @Req() req): Promise<void> {
    const admin = await this.cs.checkRoleAdmin(req.user);

    if (!admin) throw new ForbiddenException('Access Denied');
    return this.faqService.remove(+id);
  }
}
