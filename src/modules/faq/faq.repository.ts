import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateFaqDto } from './dto/create-faq.dto';
import { Faq } from './entities/faq.entity';

@EntityRepository(Faq)
export class FaqRepository extends Repository<Faq> {
  // Creating the FAQ
  async createFaq(createFaqDto: CreateFaqDto, user: User): Promise<Faq> {
    const { question, answer } = createFaqDto;

    const faq = new Faq();
    faq.question = question;
    faq.answer = answer;
    faq.user = user;

    try {
      await faq.save();
      delete faq.createdAt;
      delete faq.deletedAt;
      delete faq.updatedAt;
      return faq;
    } catch (e) {
      throw new InternalServerErrorException('Error While saving FAQ');
    }
  }

  // Listing the FaQ
  async listFaq(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('faq');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere('faq.question LIKE :search OR faq.answer LIKE :search', {
        search: `%${search}%`,
      });
    }
    // console.log(query.getSql());
    const faqs = await query.getManyAndCount();
    console.log('faqs => ', faqs);
    return faqs;
  }
}
