import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CheckVerificationCodeDto } from './dto/checkVerificationCode.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { EmailTokenDto } from './dto/email.token.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService, // private eventEmitter: EventEmitter2
  ) {}

  // Signup User
  @Post('/signup')
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  // Login User
  @Post('/signin')
  singIn(
    @Body() createUserDto: CreateUserDto,
  ): Promise<{ accessToken: string }> {
    return this.usersService.singIn(createUserDto);
  }

  // Listing all User
  @UseGuards(AuthGuard())
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.usersService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );

    return res.status(HttpStatus.OK).send(val);
    // return this.usersService.findAll(updateUserDto);
  }

  //Listing the user  by Id
  @UseGuards(AuthGuard())
  @Get(':id')
  findOne(@Param('id') id: number): Promise<User> {
    return this.usersService.findOne(id);
  }
  // updating the user Data
  @UseGuards(AuthGuard())
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  // Removing user
  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.usersService.remove(id);
  }

  // Change Password
  @UseGuards(AuthGuard('jwt'))
  @Post('chgPswd')
  changePswd(@Body() changepswdDto: ChangePasswordDto, @Req() request) {
    const user = request.user.id;
    console.log('User Id =<> => ', user);

    return this.usersService.changePswd(changepswdDto, user);
  }

  // Forgot password
  @Post('forgotPswd')
  forgotPswd(@Body() emailTokenDto: EmailTokenDto) {
    return this.usersService.forgotPswd(emailTokenDto);
  }

  //Reset Password
  @Post('resetPswd')
  resetPswd(@Body() resetPswdDto: ResetPasswordDto) {
    return this.usersService.resetPswd(resetPswdDto);
  }

  // Send SMS token
  @Post('initiate-verification')
  @UseGuards(AuthGuard('jwt'))
  async initiatePhoneNumberVerification(@Req() request) {
    console.log('Request User => ', request.user);

    if (request.user.isPhoneNumberConfirmed) {
      throw new BadRequestException('Phone number already confirmed');
    }
    await this.usersService.initiatePhoneNumberVerification(
      request.user.phoneNumber,
    );
  }

  // Check Verification
  @Post('check-verification-code')
  @UseGuards(AuthGuard('jwt'))
  async checkVerificationCode(
    @Req() request,
    @Body() verificationData: CheckVerificationCodeDto,
  ) {
    if (request.user.isPhoneNumberConfirmed) {
      throw new BadRequestException('Phone number already confirmed');
    }
    await this.usersService.confirmPhoneNumber(
      request.user.id,
      request.user.phoneNumber,
      verificationData.otp,
    );
  }

  // verify Email Token
  @Post('emailToken')
  async verifyEmail(@Body() emailTokenDto: EmailTokenDto) {
    // if (request.user.isPhoneNumberConfirmed) {
    //   throw new BadRequestException('Phone number already confirmed');
    // }
    await this.usersService.verifyEmail(emailTokenDto);
  }
}
