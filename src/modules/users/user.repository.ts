import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { EntityRepository, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import { UserRole } from './entities/user.enum';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(createUserDto: CreateUserDto): Promise<void> {
    const { firstName, lastName, email, password, emailToken, isAdmin } =
      createUserDto;

    const user = new User();
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.emailToken = emailToken;
    user.role = isAdmin ? UserRole.ADMIN : UserRole.USER;
    let salt = await bcrypt.genSalt();
    user.password = await this.hashPassword(password, salt);

    try {
      await user.save();
    } catch (error) {
      console.log(error.code);
      if (error.code === '23505') {
        throw new ConflictException('Username already exist');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  // Listing the user
  async listUser(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('users');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'users.firstName LIKE :search OR users.lastName LIKE :search OR users.phoneNumber LIKE :search OR users.email LIKE :search',
        { search: `%${search}%` },
      );
    }
    // console.log(query.getSql());
    const users = await query.getManyAndCount();
    console.log('userss => ', users);
    return users;
  }

  async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  //validating the user Password
  async validUserPassword(createUserDto: CreateUserDto): Promise<string> {
    const { email, password } = createUserDto;
    const user = await this.findOne({ email });
    console.log('User = > ', user);
    if (user && (await user.validatePassword(password))) {
      return user.email;
    } else {
      return null;
    }
  }
}
