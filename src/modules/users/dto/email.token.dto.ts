import { IsNotEmpty, IsString } from 'class-validator';

export class EmailTokenDto {
  @IsString()
  @IsNotEmpty()
  email: string;

  emailToken: string;

  firstName: string;

  emailVerify: string;
}
