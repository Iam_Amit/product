import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsString()
  @MinLength(4)
  @MaxLength(10)
  password: string;

  @IsString()
  @IsNotEmpty()
  email: string;

  emailToken: string;

  isAdmin: boolean;
}
