import { IsString, MaxLength, MinLength } from 'class-validator';

export class ResetPasswordDto {
  emailToken: string;

  updatePassword: string;
}
