import * as bcrypt from 'bcrypt';
import { type } from 'os';
import { Address } from 'src/modules/address/entities/address.entity';
import { Blog } from 'src/modules/blog/entities/blog.entity';
import { Faq } from 'src/modules/faq/entities/faq.entity';
import { Order } from 'src/modules/order/entities/order.entity';
import { Page } from 'src/modules/pages/entities/page.entity';
import { Testimonial } from 'src/modules/testimonial/entities/testimonial.entity';
import {
  BaseEntity,
  BeforeInsert,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserRole } from './user.enum';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column({ default: false })
  emailVerify: boolean;

  @Column()
  password: string;

  async validatePassword(password: string): Promise<boolean> {
    return await bcrypt.compare(password, this.password);
  }

  @Column({ nullable: true })
  phoneNumber: string;

  @Column({ default: false })
  public isPhoneNumberConfirmed: boolean;

  @Column({ default: null })
  otp: string;

  @Column({ nullable: true })
  image: string;

  @Column({ type: 'enum', enum: UserRole, default: UserRole.USER })
  role: UserRole;

  @Column({ nullable: true })
  emailToken: string;

  @DeleteDateColumn()
  deletedAt?: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column({ nullable: true })
  accessToken: String;

  @OneToMany((type) => Order, (order) => order.user)
  orders: Order[];

  @OneToMany((type) => Address, (address) => address.user)
  addresses: Address[];

  @OneToMany((type) => Blog, (blog) => blog.user)
  blogs: Blog[];

  @OneToMany((type) => Page, (page) => page.user)
  pages: Page[];

  @OneToMany((type) => Testimonial, (testimonial) => testimonial.user)
  testimonials: Testimonial[];

  @OneToMany((type) => Faq, (faq) => faq.user)
  faqs: Faq[];

  // @BeforeInsert()
  // emailToLowerCase() {
  //   this.email = this.email.toLocaleLowerCase();
  // }
}
