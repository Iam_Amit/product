import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from 'strategies/jwt.strategies';
import { MailModule } from 'src/mail/mail.module';
import { AddressRepository } from '../address/address.repository';
import { ConfigService } from '@nestjs/config';
import { MailService } from 'src/mail/mail.service';

@Module({
  imports: [
    MailModule,

    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: 'Qwertyuiop1234567890',
      signOptions: {
        expiresIn: '10d',
      },
    }),
    TypeOrmModule.forFeature([UserRepository]),
  ],

  controllers: [UsersController],
  providers: [
    UsersService,
    JwtStrategy,
    MailService,
    AddressRepository,
    ConfigService,
  ],
  exports: [MailService, UsersService, PassportModule, JwtStrategy, JwtModule],
})
export class UsersModule {}
