import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from 'src/jwt/jwt-payload-interface';
import { MailService } from 'src/mail/mail.service';
import { Twilio } from 'twilio';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { EmailTokenDto } from './dto/email.token.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UserRole } from './entities/user.enum';
import { UserRepository } from './user.repository';

@Injectable()
export class UsersService {
  private twilioClient: Twilio;
  private user: User;
  constructor(
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
    private jwtService: JwtService,
    private mailservice: MailService,
    private readonly configService: ConfigService,
  ) {
    const accountSid = configService.get('TWILIO_ACCOUNT_SID');
    const authToken = configService.get('TWILIO_AUTH_TOKEN');

    this.twilioClient = new Twilio(accountSid, authToken);
  }

  //Signup User
  async create(createUserDto: CreateUserDto) {
    createUserDto.emailToken = await Math.floor(
      10000 + Math.random() * 90000,
    ).toString();
    const createUser = await this.userRepo.createUser(createUserDto);

    const data = await this.mailservice.sendUserConfirmation(
      createUserDto.emailToken,
      createUserDto.email,
      createUserDto.firstName,
    );
    console.log('Data => ', data);

    return createUser;
  }

  // Verify Email Token
  async verifyEmail(emailTokenDto: EmailTokenDto) {
    const email = emailTokenDto.email;
    const token = emailTokenDto.emailToken;
    console.log('Email => ', email);
    console.log('Email => ', token);

    const emailExist = await this.userRepo.findOne({
      email: email,
      emailToken: token,
    });
    console.log('Existed email = > ', emailExist);

    if (!emailExist) {
      throw new NotFoundException(`User  with the email ${email} not found`);
    }

    return this.userRepo.update(
      { email },
      { emailToken: '', emailVerify: true },
    );
  }

  // User Login
  async singIn(createUserDto: CreateUserDto): Promise<{ accessToken: string }> {
    console.log('qwerrr => ');

    const email = await this.userRepo.validUserPassword(createUserDto);
    console.log('SignIN => ', email);

    if (!email) {
      throw new UnauthorizedException('Invalid Credentials');
    }
    const payload: JwtPayload = { email };
    const accessToken = await this.jwtService.sign(payload);
    return { accessToken };
  }

  // List all the user
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.userRepo.listUser(
      search,
      page,
      limit,
    );
    const total = await this.userRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      delete x.password;
      delete x.otp;
      delete x.role;
      delete x.emailToken;
      delete x.accessToken;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get all user account
  async getAllUsersCount() {
    return await this.userRepo.count({ role: UserRole.USER });
  }

  // Listing the user By Id
  async findOne(id: number): Promise<User> {
    const found = await this.userRepo.findOne(id, {
      where: { id: id },
      relations: ['addresses', 'orders'],
    });
    if (!found) {
      throw new NotFoundException(`User  with the id ${id} not found`);
    }
    return found;
  }

  // Updating the user
  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.findOne(id);
    try {
      await this.userRepo.update(id, updateUserDto);
      return {
        message: 'User Updated',
      };
    } catch (e) {
      throw new InternalServerErrorException('Something went wrong');
    }
  }

  // Removing the user
  async remove(id: number): Promise<void> {
    const result = await this.userRepo.softDelete(id);
    // const result = await this.userRepo.restore(id); In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`User  with the id ${id} not found`);
    }
  }

  // update User Password
  async changePswd(changepswdDto: ChangePasswordDto, userId) {
    const Oldpswd = await this.userRepo.findOne({});
    console.log('Old Pasword => ', Oldpswd.password);

    const oldPassword = changepswdDto.oldPassword;
    const newPassword = changepswdDto.newPassword;
    console.log('oldPassword => ', oldPassword);

    const match = await bcrypt.compare(oldPassword, oldPassword);
    const saltOrRounds = await bcrypt.genSalt();
    const newPswd = await this.userRepo.hashPassword(newPassword, saltOrRounds);
    console.log('New Paswd => ', newPswd);

    if (match) {
      console.log('Please enter the corredct OldPaswd ! ');
    }
    const userData = await this.userRepo.update(
      { id: userId },
      { password: newPswd },
    );

    return userData;
  }

  // Forgot password API
  async forgotPswd(emailTokenDto: EmailTokenDto) {
    const token = Math.floor(10000 + Math.random() * 90000).toString();

    const email = emailTokenDto.email;
    console.log('Email => ', email);

    const emailExist = await this.userRepo.findOne({ email });
    // console.log('Existed email = > ',emailExist);

    if (!emailExist) {
      throw new NotFoundException(`User  with the email ${email} not found`);
    }
    const data = await this.mailservice.sendUserConfirmation(
      token,
      emailTokenDto.email,
      emailTokenDto.firstName,
    );
    console.log('Data => ', data);
    return this.userRepo.update(
      { email: emailExist.email },
      { emailToken: token },
    );
  }

  // Reset Password
  async resetPswd(resetPswdDto: ResetPasswordDto) {
    const token = resetPswdDto.emailToken;
    console.log('Email Token => ', token);

    const matchToken = await this.userRepo.findOne({ emailToken: token });
    console.log('Match Token => ', matchToken);

    if (!matchToken) {
      throw new NotFoundException(`Enter token ${token} do not match !`);
    }

    const updatePassword = resetPswdDto.updatePassword;
    console.log('update password => ', updatePassword);

    const saltOrRounds = await bcrypt.genSalt();
    const newPswd = await this.userRepo.hashPassword(
      updatePassword,
      saltOrRounds,
    );
    console.log('New Paswd => ', newPswd);

    const userData = await this.userRepo.update(
      { emailToken: token },
      { password: newPswd },
    );

    return userData;
  }

  // Send SMS TOken
  async initiatePhoneNumberVerification(phoneNumber: string) {
    const serviceSid = await this.configService.get(
      'TWILIO_VERIFICATION_SERVICE_SID',
    );
    console.log('serviceSid => ', serviceSid);

    const token = await Math.floor(10000 + Math.random() * 90000).toString();
    console.log('Singup API ', token);

    const sendToken = await this.twilioClient.messages
      .create({
        body: `Your one time OTP for verification is: ${token}`,
        from: '+18182736764',
        to: '+91' + phoneNumber,
      })
      .then((message) => console.log(message.sid));

    console.log('sendToken => ', sendToken);

    const otpToken = await this.userRepo.update(
      { phoneNumber: phoneNumber },
      { otp: token },
    );
    console.log('otpToken => ', otpToken);
    //  return this.userRepo.update({ phoneNumber: phoneNumber }, {otp: token});
    return 'Success !';
  }

  // Verfiy SMS Token

  async confirmPhoneNumber(
    userId: number,
    phoneNumber: string,
    verificationCode: string,
  ) {
    const fndOtp = await this.userRepo.findOne({ id: userId });
    console.log('findOtp = > ', fndOtp.otp);

    if (fndOtp.otp !== verificationCode) {
      throw new BadRequestException('Wrong code provided');
    }
    await this.userRepo.update(
      { id: userId },
      { isPhoneNumberConfirmed: true },
    );
  }
}
