import { Controller, ForbiddenException, Post, Req, Res } from '@nestjs/common';
import { CommonService } from 'src/common/common.service';
import { FileUploadService } from './fileupload.service';

@Controller('file')
export class FileUploadController {
  constructor(
    private fileUploadService: FileUploadService,
    private readonly cs: CommonService,
  ) {}

  //   @UseGuards(AuthGuard('jwt'))
  //   @Post('video')
  //   @UseInterceptors(FileInterceptor('video'))
  //   async uploadedFile(@UploadedFile() file) {
  //     const response = {
  //       originalname: file.originalname,
  //       filename: file.filename,
  //     };
  //     return response;
  //   }
  @Post('upload')
  async create(@Req() request, @Res() response) {
    try {
      await this.fileUploadService.fileupload(request, response);
    } catch (error) {
      return response
        .status(500)
        .json(`Failed to upload image file: ${error.message}`);
    }
  }

  @Post('uploadVideo')
  async createVideoFile(@Req() request, @Res() response) {
    try {
      await this.fileUploadService.fileuploadVideo(request, response);
    } catch (error) {
      return response
        .status(500)
        .json(`Failed to upload image file: ${error.message}`);
    }
  }
}
