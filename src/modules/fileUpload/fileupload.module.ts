import { Module } from '@nestjs/common';
import { ConfigService } from 'aws-sdk';
import { CommonService } from 'src/common/common.service';
import { FileUploadController } from './fileupload.controller';
import { FileUploadService } from './fileupload.service';
// import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    // MulterModule.register({
    //   dest: './uploads',
    // }),
  ],
  controllers: [FileUploadController],
  providers: [FileUploadService, ConfigService, CommonService],
  exports: [FileUploadService],
})
export class FileuploadModule {}
