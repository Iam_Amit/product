import { HttpStatus, Injectable, Req, Res } from '@nestjs/common';
import * as multer from 'multer';
import * as AWS from 'aws-sdk';
import * as multerS3 from 'multer-s3';
import { ConfigService } from '@nestjs/config';
import { editFileName } from 'src/auth/constants/function';
import * as fs from 'fs';
import { S3 } from 'aws-sdk';
import { Logger } from '@nestjs/common';

// const AWS_S3_BUCKET_NAME = process.env.AWS_S3_BUCKET_NAME_IMAGES;
const s3 = new AWS.S3();
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

@Injectable()
export class FileUploadService {
  constructor() {}

  async fileupload(@Req() req, @Res() res) {
    try {
      this.uploadImg(req, res, function (error) {
        if (error) {
          console.log(error);
          return res.status(404).json(`Failed to upload image file: ${error}`);
        }
        return res.status(201).json(req.files[0].location);
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json(`Failed to upload image file: ${error}`);
    }
  }

  uploadImg = multer({
    storage: multerS3({
      s3: s3,
      bucket: 'productimages-1/images/',
      acl: 'public-read',
      key: function (request, file, cb) {
        cb(null, `${Date.now().toString()} - ${file.originalname}`);
      },
    }),
  }).array('file', 1);

  // uploading the video

  async fileuploadVideo(@Req() req, @Res() res) {
    try {
      this.uploadVideo(req, res, function (error) {
        if (error) {
          console.log(error);
          return res.status(404).json(`Failed to upload video file: ${error}`);
        }
        return res.status(201).json(req.files[0].location);
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json(`Failed to upload video file: ${error}`);
    }
  }

  uploadVideo = multer({
    storage: multerS3({
      s3: s3,
      bucket: 'productimages-1/video/',
      acl: 'public-read',
      key: function (request, file, cb) {
        cb(null, `${Date.now().toString()} - ${file.originalname}`);
      },
    }),
  }).array('uploadVideo', 1);
}
