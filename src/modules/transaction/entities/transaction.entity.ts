import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  intent: string;

  @Column()
  state: string;

  @Column()
  cart: string;

  @Column()
  total: string;

  @Column()
  merchant_id: string;

  // @
}
