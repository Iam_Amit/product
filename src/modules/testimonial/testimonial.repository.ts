import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateTestimonialDto } from './dto/create-testimonial.dto';
import { Testimonial } from './entities/testimonial.entity';

@EntityRepository(Testimonial)
export class TestimonialRepository extends Repository<Testimonial> {
  // Creating the Testimonial
  async createTestimonial(
    createTestimonialDto: CreateTestimonialDto,
    user: User,
  ): Promise<Testimonial> {
    const { title, description, rating } = createTestimonialDto;

    const testimonial = new Testimonial();
    testimonial.title = title;
    testimonial.description = description;
    testimonial.rating = rating;
    testimonial.user = user;

    try {
      await testimonial.save();
      delete testimonial.createdAt;
      delete testimonial.deletedAt;
      delete testimonial.updatedAt;
      return testimonial;
    } catch (e) {
      throw new InternalServerErrorException('Error While saving Testimonial');
    }
  }

  // Listing the Testimonial for User

  async listTestimonialUser(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('testimonial').where({
      isView: true,
    });
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'testimonial.title LIKE :search OR testimonial.description LIKE :search OR testimonial.rating LIKE :search',
        { search: `%${search}%` },
      );
    }
    console.log(query.getSql());
    const testimonials = await query.getManyAndCount();
    console.log('testimonials => ', testimonials);
    return testimonials;
  }

  // Listing the Testimonial for Admin

  async listTestimonialAdmin(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('testimonial');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'testimonial.title LIKE :search OR testimonial.description LIKE :search OR testimonial.rating LIKE :search',
        { search: `%${search}%` },
      );
    }
    // console.log(query.getSql());
    const testimonials = await query.getManyAndCount();
    console.log('testimonials => ', testimonials);
    return testimonials;
  }
}
