import { Module } from '@nestjs/common';
import { TestimonialService } from './testimonial.service';
import { TestimonialController } from './testimonial.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TestimonialRepository } from './testimonial.repository';

@Module({
  imports: [TypeOrmModule.forFeature([TestimonialRepository])],
  controllers: [TestimonialController],
  providers: [TestimonialService],
})
export class TestimonialModule {}
