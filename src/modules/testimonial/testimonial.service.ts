import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { CreateTestimonialDto } from './dto/create-testimonial.dto';
import { HideShowTestimonialDto } from './dto/update-hideSho-testimonial.dto';
import { UpdateTestimonialDto } from './dto/update-testimonial.dto';
import { Testimonial } from './entities/testimonial.entity';
import { TestimonialRepository } from './testimonial.repository';

@Injectable()
export class TestimonialService {
  constructor(
    @InjectRepository(TestimonialRepository)
    private testimonialRepo: TestimonialRepository,
  ) {}

  // Creating the Testimonial
  create(createTestimonialDto: CreateTestimonialDto, user: User) {
    return this.testimonialRepo.createTestimonial(createTestimonialDto, user);
  }

  // Listing the testimonials for user
  async findAllUser(page: number, limit: number, search: string) {
    const [output, searchTotal] =
      await this.testimonialRepo.listTestimonialUser(search, page, limit);
    const total = await this.testimonialRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }
  // Listing the testimonial for Admin
  async findAllAdmin(page: number, limit: number, search: string) {
    const [output, searchTotal] =
      await this.testimonialRepo.listTestimonialAdmin(search, page, limit);
    const total = await this.testimonialRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Listing the testimonils by Id
  async findOne(id: number, req): Promise<Testimonial> {
    const userName = req.user.fullname;
    console.log('UserName = > ', userName);
    const found = await this.testimonialRepo.findOne(id, {
      // where: { id: id },
      // relations: ['user', 'product', 'component'],
      // select: ['description', 'status', 'title'],
    });
    if (!found) {
      throw new NotFoundException(`Testimonial  with the id ${id} not found`);
    }
    return found;
  }
  // hide_show API
  updateHideShow(id: number, hideShowTestDto: HideShowTestimonialDto) {
    console.log('qwertrew => ', typeof hideShowTestDto.isView);
    return this.testimonialRepo.update(id, hideShowTestDto);
  }

  // update the testimonial data
  update(id: number, updateTestimonialDto: UpdateTestimonialDto) {
    return this.testimonialRepo.update(id, updateTestimonialDto);
  }

  async remove(id: number) {
    const result = await this.testimonialRepo.softDelete(id);
    // const result = await this.testimonialRepo.restore(id); //In case of restore

    if (result.affected === 0) {
      throw new NotFoundException(`Testimonial  with the id ${id} not found`);
    }
  }
}
