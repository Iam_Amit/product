import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  Query,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { TestimonialService } from './testimonial.service';
import { CreateTestimonialDto } from './dto/create-testimonial.dto';
import { UpdateTestimonialDto } from './dto/update-testimonial.dto';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { HideShowTestimonialDto } from './dto/update-hideSho-testimonial.dto';

@Controller('testimonial')
@UseGuards(AuthGuard('jwt'))
export class TestimonialController {
  constructor(private readonly testimonialService: TestimonialService) {}

  // Create Testimonial
  @Post()
  create(@Body() createTestimonialDto: CreateTestimonialDto, @Req() req) {
    console.log(' User => ', req.user.id);
    return this.testimonialService.create(createTestimonialDto, req.user.id);
  }

  // for the user
  @Get('/user')
  async findAllUser(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.testimonialService.findAllUser(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );

    return res.status(HttpStatus.OK).send(val);
    // return this.testimonialService.findAll(createTestimonialDto);
  }

  // for the Admin interface
  @Get('/admin')
  async findAllAdmin(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    const val = await this.testimonialService.findAllAdmin(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );

    return res.status(HttpStatus.OK).send(val);
    // return this.testimonialService.findAll(createTestimonialDto);
  }

  @Get(':id')
  findOne(@Param('id') id: number, @Req() req) {
    // console.log('UserId => ', req.user.firstName, req.user.lastName);
    // console.log('User fullName ', req.user.fullname);
    return this.testimonialService.findOne(+id, req);
  }
  // Hide_show API
  @Patch('hide_show/:id')
  updateHideShow(
    @Param('id') id: number,
    @Body() hideShowTestDto: HideShowTestimonialDto,
  ) {
    console.log('bodydata => ', hideShowTestDto);
    return this.testimonialService.updateHideShow(+id, hideShowTestDto);
  }

  // update the testimonial Data
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTestimonialDto: UpdateTestimonialDto,
  ) {
    return this.testimonialService.update(+id, updateTestimonialDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.testimonialService.remove(+id);
  }
}
