import { type } from 'os';
import { Component } from 'src/modules/component/entities/component.entity';
import { Product } from 'src/modules/product/entities/product.entity';
import { User } from 'src/modules/users/entities/user.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Testimonial extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  rating: string;

  // @Column({
  //   type: 'enum',
  //   enum: TestimonialStatus,
  //   default: [TestimonialStatus.zero],
  // })
  // type: TestimonialStatus;

  @Column({ default: true, type: 'boolean' })
  isView: boolean;

  @DeleteDateColumn()
  deletedAt?: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne((type) => User, (user) => user.testimonials)
  user: User;

  @ManyToOne((type) => Product, (product) => product.testimonials)
  product: Product;

  @ManyToOne((type) => Component, (component) => component.testimonials)
  component: Component;
}
