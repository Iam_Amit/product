import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/modules/users/user.repository';
import { UsersModule } from 'src/modules/users/users.module';
import { UsersService } from 'src/modules/users/users.service';
import { AuthService } from './auth.service';
import { RolesGuard } from './guards/roles.guard';

@Module({
  imports: [UsersModule, TypeOrmModule.forFeature([UserRepository])],

  providers: [AuthService, UsersService, RolesGuard],
  exports: [AuthService, RolesGuard],
})
export class AuthModule {}
