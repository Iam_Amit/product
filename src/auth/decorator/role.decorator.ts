import { SetMetadata } from '@nestjs/common';

export const hasRoles = (...hasRoles: any) => SetMetadata('roles', hasRoles);
