import {
  CanActivate,
  ExecutionContext,
  forwardRef,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { User } from 'src/modules/users/entities/user.entity';
import { UserRole } from 'src/modules/users/entities/user.enum';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler()); //admin || [amdin,user]
    // console.log(' Roles 1 => ', roles)

    if (!roles) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    console.log('Request => ', request.user);
    const user: User = request.user;
    console.log('User  => ', user);
    return matchRoles(roles, user.role);
  }
}

function matchRoles(
  roles: string[],
  role: UserRole,
): boolean | Promise<boolean> {
  console.log('matchRoles => ', matchRoles);
  return roles.includes(role);
}
