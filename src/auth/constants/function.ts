import { extname } from 'path';

export const imageFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(new Error('Only image files are allowed!'), false);
  }
  callback(null, true);
};

export const editFileName = (file, callback) => {
  const name = file.split('.')[0];
  const fileExtName = extname(file);
  const randomName = Array(10)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, [
    `${name}-${randomName}${fileExtName}`,
    `${fileExtName.slice(1, fileExtName.length)}`,
  ]);
};
