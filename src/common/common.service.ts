import { Injectable } from '@nestjs/common';
import { User } from 'src/modules/users/entities/user.entity';
import { UserRole } from 'src/modules/users/entities/user.enum';

@Injectable()
export class CommonService {
  randomNumber(length: number) {
    const string = '0123456789';
    return this.iterateString(string, length);
  }

  randomString(length: number) {
    const string =
      '0123456789qwertyuioplkjhgfdsazxcvbnmMNBVCXZASDFGHJKLPOIUYTREWQ';
    return this.iterateString(string, length);
  }

  iterateString(string, length) {
    const stringLength = string.length;
    let generatedValue = '';
    while (generatedValue.length < length) {
      generatedValue += string.charAt(Math.floor(Math.random() * stringLength));
    }
    return generatedValue;
  }

  checkRoleAdmin(user: User) {
    if (user.role === UserRole.ADMIN) {
      return true;
    }
  }

  checkRoleUser(user: User) {
    if (user.role === UserRole.USER) {
      return true;
    }
  }
}
