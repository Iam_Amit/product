import { Module } from '@nestjs/common';
import { CommonService } from './common.service';

@Module({
  //   controllers: [DashboardController],
  providers: [CommonService],
  exports: [CommonService],
})
export class CommonModule {}
