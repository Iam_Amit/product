import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
// import { MailController } from './mail.controller';
import { join } from 'path';
import { User } from 'src/modules/users/entities/user.entity';

@Module({
  imports: [
    MailerModule.forRoot({
      // transport: 'smtps://user@example.com:topsecret@smtp.example.com',
      // or
      transport: {
        // host: 'smtp',
        host: 'localhost',
        port: 3000,
        service: 'gmail',
        secure: true,
        auth: {
          user: 'unofficial.vcrew@gmail.com',
          pass: 'newPass@123',
        },
      },
      defaults: {
        from: 'unofficial.vcrew@gmail.com',
        // to: ``,
        // subject: ``,
        // text: ``,
      },
      template: {
        dir: join(__dirname, '../../mail/templates'),
        adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
        options: {
          strict: true,
        },
      },
    }),
  ],
  // controllers: [MailController],
  providers: [MailService, User],
  exports: [MailService],
})
export class MailModule {}
