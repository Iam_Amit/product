import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserConfirmation(
    emailToken: string,
    email: string,
    firstName: string,
  ) {
    const url = `example.com/auth/confirm?emailToken=${emailToken}`;
    console.log('URL => ', url);

    await this.mailerService.sendMail({
      to: email,
      from: 'unofficial.vcrew@gmail.com',
      subject: 'please have a look to email !',
      template: './/forgotPassword',
      context: {
        emailToken,
        firstName: firstName,
        url,
      },
    });
  }

  // async sendUserUpdateProfile(email: string, firstName: string){
  //   const url = `example.com/auth/confirm?token`;
  //   console.log('URL => ',url);

  //   await this.mailerService.sendMail({

  //     to: email,
  //     from: 'unofficial.vcrew@gmail.com',
  //     subject: 'please have a look to email !',
  //     template: './confirmation',
  //     context: {
  //       firstName: firstName,
  //       url,

  //     },
  //   });
  // }
}
