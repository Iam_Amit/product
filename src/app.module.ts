import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as Joi from 'joi';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import { ConfigurationsModule } from './configurations/configurations.module';
import { MailModule } from './mail/mail.module';
import { AddressModule } from './modules/address/address.module';
import { BlogModule } from './modules/blog/blog.module';
import { ComponentModule } from './modules/component/component.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { FaqModule } from './modules/faq/faq.module';
import { FileuploadModule } from './modules/fileUpload/fileupload.module';
import { OrderModule } from './modules/order/order.module';
import { PageModule } from './modules/pages/page.module';
import { ProductModule } from './modules/product/product.module';
import { TestimonialModule } from './modules/testimonial/testimonial.module';
import { TransactionModule } from './modules/transaction/transaction.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [
    MulterModule.register({
      dest: './uploads',
    }),
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
      validationSchema: Joi.object({
        TWILIO_SENDER_PHONE_NUMBER: Joi.string().required(),
        TWILIO_ACCOUNT_SID: Joi.string().required(),
        TWILIO_AUTH_TOKEN: Joi.string().required(),
        TWILIO_VERIFICATION_SERVICE_SID: Joi.string().required(),
      }),
    }),
    // EventEmitterModule.forRoot(),

    //   TewilioModule.forRoot({
    //   accountSid: process.env.TWILIO_ACCOUNT_SID,
    //   authToken: process.env.TWILIO_AUTH_TOKEN,
    // }),

    UsersModule,
    BlogModule,
    FaqModule,
    // PaymentModule,
    ProductModule,
    FileuploadModule,
    ComponentModule,
    OrderModule,
    AddressModule,
    TestimonialModule,
    PageModule,
    MailModule,

    TypeOrmModule.forRoot({
      type: 'postgres',
      port: 5432,
      host: 'localhost',
      username: 'postgres',
      password: 'Qwerty@!#456',
      // logging: 'all',
      // logger: 'advanced-console',
      database: 'e_product',
      entities: [join(__dirname, './**/**.entity{.ts,.js}')],
      migrationsTableName: 'Migrations_History',
      synchronize: true,
    }),
    // MediaModule,
    TransactionModule,
    DashboardModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
